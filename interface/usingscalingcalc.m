function out=usingscalingcalc(sample)

%sample is a list of samples with one row per sample and 3 columns for
%latitude, longitude, and elevation
%scaling model choices are:
%'DU'  'DE'  'LI'  'SA'  'SF'  'LM'  'ST' or 'all'
numbersamples=size(sample,1);

for i=1:numbersamples;
scaling=scalingcalc(sample(i,1),sample(i,2),sample(i,3));

out.ST(i)=scaling.StSel10;
out.DU(i)=scaling.DuSel10;
out.LI(i)=scaling.LiSel10;
out.LM(i)=scaling.LmSel10;
out.DE(i)=scaling.DeSel10;
out.SF(i)=scaling.SfSel10;
out.SA10(i)=scaling.SaSel10;
out.SA26(i)=scaling.SaSel26;
out.SA14(i)=scaling.SaSel14;
out.SA3(i)=scaling.SaSel3;
out.SA21(i)=scaling.SaSel21;
out.SA36ca(i)=scaling.SaSel36Ca;
out.SA36k(i)=scaling.SaSel36K;
out.SA36ti(i)=scaling.SaSel36Ti;
out.SA36fe(i)=scaling.SaSel36Fe;
out.SA36th(i)=scaling.SaSFth;
out.SA36eth(i)=scaling.SaSFeth;

end