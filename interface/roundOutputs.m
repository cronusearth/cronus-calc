function [age_string, uncert_string, internal_string] = roundOutputs(age_value, uncert_value, internal_uncert, rounding_decimal)
	% Sigfig rounding is causing issues, so we will just round to 10s of years
	age_string = round(age_value, rounding_decimal)
	uncert_string = round(uncert_value, rounding_decimal)
	internal_string = round(internal_uncert, rounding_decimal)
end

