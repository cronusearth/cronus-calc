function [sample, outputdata, plotdata, labeldata] = he_3(inputs, RawInputs, sampleInputs, OutputData, sampleCount, calcConfig)
	% Wrapper for he-3 calculator
	lprint(calcConfig.logFile,'Starting he_3\n');
	totalSampleData = [];
	totalSampleErrData = [];
	
	for i=1:sampleCount;
		sampleInput = sampleInputs(i);
		% construct the input vector (order is important here).
		% I (hakan) don't recommend getting the input as such. We already have
		% all input in distinct variables so we can easily use them.
		sampleData = [ 	sampleInput.lat
						sampleInput.long
						sampleInput.elev
						sampleInput.ATM
						sampleInput.sthck
						sampleInput.bd
						sampleInput.shieldingF
						sampleInput.erosionrate
						sampleInput.heconc
						sampleInput.inheritance
						sampleInput.lambdafe
						sampleInput.depthtosample
						sampleInput.yearsampled
					]; 
		% input vector for uncertainities (order is important here)
		sampleErrData = [sampleInput.lat_uncert
						sampleInput.long_uncert
						sampleInput.elev_uncert
						sampleInput.ATM_uncert
						sampleInput.sthck_uncert
						sampleInput.bd_uncert
						sampleInput.shieldingF_uncert
						sampleInput.erosionrate_uncert
						sampleInput.heconc_uncert
						sampleInput.inheritance_uncert
						sampleInput.lambdafe_uncert
						sampleInput.depthtosample_uncert
						sampleInput.yearsampled_uncert
					  ];
		totalSampleData{end+1} = [sampleData];
		totalSampleErrData{end+1} = [sampleErrData];
	end
	
	%Load running_times and overwrite running_times if it exists
	%Create running_times for he
	running_times = struct();
	
	running_times_default.he.depth.average = 0;
	running_times_default.he.depth.count = 0;
	running_times_default.he.surface.DE.average = 0; running_times_default.he.surface.DE.count = 0;
	running_times_default.he.surface.DU.average = 0; running_times_default.he.surface.DU.count = 0;
	running_times_default.he.surface.LI.average = 0; running_times_default.he.surface.LI.count = 0;
	running_times_default.he.surface.LM.average = 0; running_times_default.he.surface.LM.count = 0;
	running_times_default.he.surface.SA.average = 0; running_times_default.he.surface.SA.count = 0;
	running_times_default.he.surface.ST.average = 0; running_times_default.he.surface.ST.count = 0;
	running_times_default.he.surface.SF.average = 0; running_times_default.he.surface.SF.count = 0;
	if (exist('running_times.mat') == 2)
		load('running_times','running_times');
	end
	%Merge the loaded variable with the defaults above in case they don't exist
	warning off;
	running_times = catstruct(running_times_default,running_times);
	warning on;
	
	%Call surface code, then add sample name and plotfiles to output
	[sample, outputdata, plotdata, plotfiles, labeldata,running_times] = he_3_surface(inputs, RawInputs, totalSampleData, totalSampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig);
	for i=1:sampleCount;
		sample(i).name = sampleInputs(i).name;
		sample(i).plot = plotfiles{i};
	end
	%Write out running_times
	save('running_times','running_times');
end

function [samples, outputdata, plotdata, plotfiles, labeldata, running_times] = he_3_surface(inputs, RawInputs, sampleData, sampleErrData, sampleInputs, sampleCount, OutputData, running_times, calcConfig)
	samples = repmat(struct([]),sampleCount,1);
	outputdata = [];
	plotdata = [];
	labeldata = [];
	plotfiles = {};
	for i=1:sampleCount;
		ElementID = tic;
		lprint(calcConfig.logFile,'************************************************\n');
		lprint(calcConfig.logFile,'He Sample %d/%d (%s) Starting (average execution time for %s is %s)...\n',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling,sec2hms(running_times.he.surface.(RawInputs(i).scaling).average));
		lprint(calcConfig.logFile,['Using scaling ' lower(RawInputs(i).scaling) '\n']);
		if (running_times.he.surface.(RawInputs(i).scaling).count > 0)
			lprint(calcConfig.userLogFile,'He Sample %d/%d (%s) (average execution time for %s is %s)...',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling,sec2hms(running_times.he.surface.(RawInputs(i).scaling).average));
		else
			lprint(calcConfig.userLogFile,'He Sample %d/%d (%s) (%s)...',i,sampleCount,sampleInputs(i).name,RawInputs(i).scaling);
		end
		lastwarn('');
		
		if (calcConfig.debug)
			lprint(calcConfig.logFile,'Generating debug data...\n');
			outvector = 1:100;times = 1:100;plotprod = 1:1:100;derivs = 100:-1:1;
		else
			path = genpath(fullfile(pwd,'..',lower(RawInputs(1).scaling)));
			lprint(calcConfig.logFile,'Adding scaling scheme to matlab path...\n');
				addpath( path );
			lprint(calcConfig.logFile,'Calling he3age\n');
				[outvector,times,plotprod] = he3age(sampleData{i}, sampleErrData{i}, RawInputs(i).scaling);
			lprint(calcConfig.logFile,'Done Calling he3age\n');
			%Clean up the path
			lprint(calcConfig.logFile,'Removing scaling scheme from matlab path...\n');
				rmpath( path );
		end


		%outputs
		% 1. age (kyr)
		% 2. age uncertainty 
		% 3. Contemporary Elevation/latitude scaling factor for neutrons for He (unitless)
		% 4. Contemporary depth avg prod rate, neutron spallation (atoms/g/yr)
		% 5. Qs (unitless)
		% 6. Inherited 3-He (atoms/g of target)
		% 7. Measured 3-He (atoms/g of target)
		% 8. Analytical (internal) uncertainty (kyr)
		
		lprint(calcConfig.logFile,'Generating labels and output data...\n');
		labeldata = {'Sample Name'};
		
		% Construct output structure for this sample
		[sample_age, sample_uncert, internal_uncert] = roundOutputs(outvector(1),outvector(2),outvector(8),2);

		samples(i).age = num2str(sample_age);
		labeldata{end+1} = 'Age';

		samples(i).uncertainty = num2str(sample_uncert);
		labeldata{end+1} = 'Age Uncertainty';

		samples(i).erosionrate = num2str(sampleInputs(i).erosionrate);
		labeldata{end+1} = 'Erosion Rate';

		samples(i).elsfneutron = num2str(outvector(3));
		labeldata{end+1} = 'Scaling He Spallation';

		samples(i).prHe = num2str(outvector(4));
		labeldata{end+1} = 'Production He Spallation';

		samples(i).qs = num2str(outvector(5));
		labeldata{end+1} = 'Qs';
		
		samples(i).internalUncertainty = num2str(internal_uncert);
		labeldata{end+1} = 'Analytical (Internal) Uncertainty';

		if(length(lastwarn)>0); samples(i).warning = lastwarn; end;

		% Generate Plots
		figure;
		hold all;
		plot(times,plotprod);
		xlabel('Time Before 2010 (ka)');
		ylabel('Production Rate (atoms/g sample/yr)');
		hold off;
		plotfile = [char(inputs.prefix) '_' num2str( i ) '_' sampleInputs(i).name '_he.png'];
		set(gcf,'PaperPositionMode','auto');
		lprint(calcConfig.logFile,'Saving plots...\n'); 
		saveas(gcf,[OutputData.tempDir '/' plotfile]);
		plotfiles{end+1} = plotfile;

		% Generate Output Data
		lprint(calcConfig.logFile,'Generating Output Data...\n');   
		cellstruct = struct2cell(samples(i));
		outputdata = [outputdata '\n' sampleInputs(i).name ',' sprintf('%s,',cellstruct{:}) ];
		labeldata = [sprintf('%s,',labeldata{:}) ];
		plotdata = [plotdata '\n' sampleInputs(i).name '\n' ...
					 'Times Before 2010,' sprintf('%d,',times) '\n' ...
					 'ProdHe,' sprintf('%d,',plotprod) '\n' ];
    
		element_time = toc(ElementID);
		lprint(calcConfig.logFile,['Element completed in ' sec2hms(element_time) '\n']); 
		lprint(calcConfig.logFile,'************************************************\n');
		lcprint(calcConfig.userLogFile,' %s +/- %s ka completed in %s\n',samples(i).age, samples(i).uncertainty, sec2hms(element_time));
		
		if (~calcConfig.debug)
			running_times.he.surface.(RawInputs(i).scaling).average = (running_times.he.surface.(RawInputs(i).scaling).average*running_times.he.surface.(RawInputs(i).scaling).count + element_time)/(running_times.he.surface.(RawInputs(i).scaling).count + 1);
			if (running_times.he.surface.(RawInputs(i).scaling).count < 100000) running_times.he.surface.(RawInputs(i).scaling).count = running_times.he.surface.(RawInputs(i).scaling).count + 1; end
		end
	end
end
