function [output,cache]=topo_calc(inputs,cache)
% Topographic Shielding Calculator

%set up command logging (no data is stored)
userLogFile = [char(inputs.tempDir) '/' inputs.prefix '_topo.txt'];
logFile = [char(inputs.logDir) '/' inputs.prefix];
logFile = [logFile '_topo.log'];

lprint(logFile,'Starting topo_calc\n');
lprint(userLogFile,'Sample Preprocessing Starting...');

ticID = tic;
OutputData.temp_dir = relativepath(char(inputs.tempDir));
OutputData.date = datestr(now);

tic;
lprint(logFile,'Loading cache...');
if(isempty(cache))
   % Load transformer and validator objects into the cache
   cache.transformer = cronusCalculators.xml.XMLTransformer(inputs.stylesheetFile);
   cache.validator = cronusCalculators.xml.XMLValidator(inputs.schemaFile);
end
lcprint(logFile,['    Cache loaded in ' num2str(toc) '\n']); 

% Parse input HTTP parameter sampleData as a Java XML DOM object
tic;
lprint(logFile,'Parsing inputs...');
try
   inputXMLDOM = cronusCalculators.xml.XMLModule.parseXMLstring(inputs.sampleData);
catch
   error('Input is not a well-formed XML!');
end
lcprint(logFile,['    Inputs parsed in ' num2str(toc) '\n']); 

% Validate the input against the schema file. If validation fails, a Java
% exception is thrown here and caught by the servlet.
tic;
lprint(logFile,'Validating inputs against schema...');
cache.validator.updateSchemaIfNewer;
try
   cache.validator.validate(inputXMLDOM);
catch exception
   error(strcat('Input is a well-formed XML but not a valid input for this calculator. ',getReport(exception)));
end
lcprint(logFile,['    Inputs validated in ' num2str(toc) '\n']); 

output = [];

% Loop through each sample element in the input XML
root = inputXMLDOM.getFirstChild;
children = root.getChildNodes;

lprint(logFile,'Looping through elements...\n');
lcprint(userLogFile,'    Completed in %s seconds\n',num2str(toc(ticID)));

addpath(genpath(fullfile('..','common')));
for i=0:children.getLength-1;
   ElementID = tic;

   lprint(logFile,'************************************************\n');
   lprint(logFile,'Element %d\n',i);

   sampleNode = children.item(i);
   [sampleStruct,sampleAttribs] = node2struct(sampleNode);

   lprint(userLogFile,'Sample %d (%s) Starting...',i,sampleStruct.input.name);

   % doublefields function tries to parse every field of the input struct
   % into double. Second argument (true) tells it to ignore non-numeric
   % and non-parseable fields.
   sampleInput = doublefields(sampleStruct.input,true);

   % now sampleInput contains all input data in double type
   sampleInput.theta = str2num(sampleStruct.input.theta);
   sampleInput.horizon = str2num(sampleStruct.input.horizon);
   sampleInput.snow_shielding = 1; % copied from topofactor.m
   sampleInput.apparentAttenuationLength = 152; % copied from topofactor.m
   if (all(isfield(sampleInput, {'lat', 'long', 'elevation', 'pressure'})))
      % Elev/Pressure copied from common_age.m
      if (strcmp(sampleStruct.input.pressure_elevOption,'Elevation'))
         sampleInput.elev = sampleInput.elevation;
         sampleInput.ATM = ERA40atm(sampleInput.lat, sampleInput.long, sampleInput.elev);
         sampleStruct.input.pressure = num2str(sampleInput.ATM);
         sampleAttribs.input.pressure.ft = [ sampleAttribs.input.pressure.ft ' (calculated from elevation)'];
      elseif (strcmp(sampleStruct.input.pressure_elevOption,'Pressure'))
         sampleInput.ATM = sampleInput.pressure;
         sampleInput.elev = Ptoelev( sampleInput.lat, sampleInput.long, sampleInput.pressure );
         sampleStruct.input.elevation = num2str(sampleInput.elev);
         sampleAttribs.input.elevation.ft = [ sampleAttribs.input.elevation.ft ' (calculated from pressure)'];
      else
         sampleInput.elev = sampleInput.elevation;
         sampleInput.ATM = sampleInput.pressure;
      end
      addpath(genpath(fullfile('..','sa'))); % Needed for physpars call in attenuationlength
      sampleInput.apparentAttenuationLength = attenuationlength(sampleInput.lat, sampleInput.long, sampleInput.elev, sampleInput.ATM);
      rmpath(genpath(fullfile('..','sa')));
   end

   inputcellstruct = struct2cell(sampleStruct.input);

   tic;
   lprint(logFile,'Calling topocalc code...');
	output = topofactor([sampleInput.theta; sampleInput.horizon]',...
				sampleInput.dip_direction, sampleInput.dip_angle,...
				'Ssnow',sampleInput.snow_shielding, 'Lf_a', sampleInput.apparentAttenuationLength);
   lcprint(logFile,['    Topocalc code finished in ' num2str(toc) '\n']); 

   output.name = sampleStruct.input.name;
   outChild = struct2node(inputXMLDOM,output,'output');
   sampleNode.appendChild(outChild);


   element_time = toc(ElementID);
   lprint(logFile,['Element completed in ' num2str(element_time) '\n']); 
   lprint(logFile,'************************************************\n');
   if (element_time > 3600)
      lcprint(userLogFile,'    Completed in %s hours\n',num2str(element_time/3600));
   elseif (element_time > 60)
      lcprint(userLogFile,'    Completed in %s minutes\n',num2str(element_time/60));
   else
      lcprint(userLogFile,'    Completed in %s seconds\n',num2str(element_time));
   end;
end;
rmpath(genpath(fullfile('..','common')));

tic;
lprint(logFile,'Processing results...\n');
OutputData.time_elapsed = toc(ticID);
outputDataChild = struct2node(inputXMLDOM,OutputData,'output_data');
inputXMLDOM.getFirstChild.appendChild(outputDataChild);


% Calculation finished.

% Transform the XML to a HTML page using xslt we loaded into the cache
cache.transformer.updateStylesheet;
%cache.transformer.updateStylesheetIfNewer;
resHTML = cache.transformer.transform(inputXMLDOM);

out = java.io.BufferedWriter(java.io.FileWriter([char(inputs.tempDir) '/' char(inputs.prefix) '_topo.html']));
out.write(resHTML);
out.close();

% Remove 'ROOT' from path (since ROOT is hidden in the urls)
temp_html = regexprep(OutputData.temp_dir,'ROOT\/','');
output.write = ['<html><head><meta http-equiv=''refresh'' content=''0; url=' temp_html char(inputs.prefix) '_topo.html''></head></html>'];
output.contentType = 'html';
lcprint(userLogFile,'\n\nProcessing Complete - output can be viewed <a href="%s">here</a>\n',[temp_html char(inputs.prefix) '.html']);

%End logging
diary off
end 
