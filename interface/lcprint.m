function lcprint(logFile,msg,varargin)
   if (isempty(logFile)) return; end;
   logId = fopen(logFile,'a');
   if (numel(varargin) > 0)
      Message = sprintf(msg,varargin{:});
   else
      Message = msg;
   end
   fprintf(logId, Message);
   fprintf(Message);
   fclose(logId);
end


