function world_map(latitudes, longitudes, nuclide, localmapdir, localmapfilename, logFile)
   MapUpdateID = tic;
	lprint(logFile,'Updating maps...\n');
	
	
   WorldData.(nuclide).lat = [];
   WorldData.(nuclide).long = [];
   
   dateCells = num2cell(clock);
   [year month day hour minute seconds] = dateCells{:};
   year = ['Y' num2str(year, '%04d')];
   month = ['M' num2str(month, '%02d')];
   day = ['D' num2str(day, '%02d')];
   hour = ['h' num2str(hour, '%02d')];
   minute = ['m' num2str(minute, '%02d')];
   LogData.(nuclide).(year).(month).(day).(hour).(minute).samples = 0;
   LogData.(nuclide).(year).(month).(day).(hour).(minute).batches = 0;
   
   LineColor.cl = [0.0 1.0 0.0];
   LineColor.albe = [1.0 0.0 0.0];
   LineColor.he = [0.0 0.0 1.0];
   LineColor.c = [75.0/255.0 0.0 130.0/255.0];
   LineColor.ne = [1.0 0.0 1.0];
   MadeChangeToData = 0;
   if (exist('lat_long_data.mat') == 2)
      lprint(logFile,'Loading lat/long data...\n');
      load('lat_long_data','WorldData');
   end
   if (exist('log_data.mat') == 2)
      lprint(logFile,'Loading metrics data...\n');
      load('log_data','LogData');
   end
   lprint(logFile,'Determining if stored lat/long data needs to be updated...\n');
   if (~isfield(WorldData,nuclide) || isempty(WorldData.(nuclide).lat))
      lprint(logFile,'Generating initial lat/long data...\n');
      WorldData.(nuclide).lat = latitudes;
      WorldData.(nuclide).long = longitudes;
      MadeChangeToData = 1;
   end
   
   if (~isfield(LogData, nuclide) ||...
       ~isfield(LogData.(nuclide), year) ||...
       ~isfield(LogData.(nuclide).(year), month) ||...
       ~isfield(LogData.(nuclide).(year).(month), day) ||...
       ~isfield(LogData.(nuclide).(year).(month).(day), hour) ||...
       ~isfield(LogData.(nuclide).(year).(month).(day).(hour), minute))
      LogData.(nuclide).(year).(month).(day).(hour).(minute).samples = 0;
      LogData.(nuclide).(year).(month).(day).(hour).(minute).batches = 0;
   end
   
   LogData.(nuclide).(year).(month).(day).(hour).(minute).samples = LogData.(nuclide).(year).(month).(day).(hour).(minute).samples + size(latitudes,2);
   LogData.(nuclide).(year).(month).(day).(hour).(minute).batches = LogData.(nuclide).(year).(month).(day).(hour).(minute).batches + 1;
   
   for i=1:size(latitudes,2),
      lat_exists = WorldData.(nuclide).lat == latitudes(i);
      long_exists = WorldData.(nuclide).long == longitudes(i);
      already_exists = any(lat_exists & long_exists);
      if (~already_exists)
         lprint(logFile,['Adding new lat/long data (' num2str(latitudes(i)) ', ' num2str(longitudes(i)) ')...']);
         WorldData.(nuclide).lat = [WorldData.(nuclide).lat latitudes(i)];
         WorldData.(nuclide).long = [WorldData.(nuclide).long longitudes(i)];
         MadeChangeToData = 1;
      end
   end

   % world map in Mercator projection
   fname = '773px-Mercator-projection.jpg';
   img = imread(fname);

   lprint(logFile,'Generating local map of lat/long data...');
   mapid = tic;
   savemaptofile( {latitudes}, {longitudes}, {LineColor.(nuclide)}, img, fullfile( localmapdir, localmapfilename ) );
   lcprint(logFile,['    Generated in ' num2str(toc(mapid)) '\n']);

   if (MadeChangeToData == 1)
      lprint(logFile,'Updating world map...\n');
      plotfile = fullfile( localmapdir, '../worldmap.png' );
      world_lat = {};
      world_long = {};
      line_color = {};
      nuclides = fieldnames(WorldData);
      for i=1:numel(nuclides),
         world_lat{end+1} = WorldData.(nuclides{i}).lat;
         world_long{end+1} = WorldData.(nuclides{i}).long;
         line_color{end+1} = LineColor.(nuclides{i});
      end
      savemaptofile(world_lat,world_long,line_color,img,plotfile);
      lprint(logFile,'Saving new lat/long data...\n');
      save('log_data','LogData');
   end
   
   lprint(logFile,'Saving metrics data...\n');
   save('log_data','LogData');
   
   lcprint(logFile,['    Map updated in ' num2str(toc(MapUpdateID)) '\n']); 
end

function [x,y] = mercatorProjection(lat, lon, width, height)
    x = mod((lon+180)*width/360, width) ;
    y = height/2 - log(tan((lat+90)*pi/360))*width/(2*pi);
end

function savemaptofile(latitudes,longitudes,linecolor,img,filename)
      [imgH,imgW,~] = size(img);

      figure;
      image(img);
      hold on;
      for i=1:size(latitudes,2),
         % Mercator projection
         [x,y] = mercatorProjection(latitudes{i}, longitudes{i} - 360, imgW, imgH);

         % plot markers on map
         plot(x,y,'.', 'MarkerEdgeColor',linecolor{i}, 'MarkerSize',30);
      end
      axis off;
      hold off;

      set(gca, 'LooseInset', [0 0 0 0]);
      ti = get(gca,'TightInset');
      set(gca,'Position',[ti(1) ti(2) 1-ti(3)-ti(1) 1-ti(4)-ti(2)]);
      set(gca,'units','centimeters');
      pos = get(gca,'Position');

      set(gcf, 'PaperUnits','centimeters');
      set(gcf, 'PaperSize', [pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);
      set(gcf, 'PaperPositionMode', 'manual');
      set(gcf, 'PaperPosition',[0 0 pos(3)+ti(1)+ti(3) pos(4)+ti(2)+ti(4)]);

      set(gcf, 'renderer', 'painters');
      print(gcf,'-dpng',filename);
end

