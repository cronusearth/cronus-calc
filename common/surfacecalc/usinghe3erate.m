
function [ total, erates, uncerts, eratesmm, uncertsmm ] = usinghe3erate( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );

%
% Get basic info about the sample ages.
%
nsamples=size(nominal3,1);
%
% Update the uncertainties on the concentration.
%
%
erates=zeros(nsamples,1);
uncerts=zeros(nsamples,1);
eratesmm=zeros(nsamples,1);
uncertsmm=zeros(nsamples,1);
timing=zeros(nsamples,1);
for k=1:nsamples;
  tic;
  [erates(k),uncerts(k),eratesmm(k),uncertsmm(k)] = ...
    he3erate(nominal3(k,:),uncerts3(k,:),scaling_model);
 timing(k)=toc;
  fprintf(1,'He Sample %d, computed erate=%f +- %f, %f in mm/kyr in %f minutes\n',full(...
      [k; erates(k); uncerts(k); eratesmm(k); toc/60]));
end
%

total(:,1)=erates;
total(:,2)=uncerts;
total(:,3)=eratesmm;
total(:,4)=uncertsmm;
total(:,5)=timing;

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end