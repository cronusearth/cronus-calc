function [ total, computedages, computeduncerts, scalefactorsca, scalefactorsk, ...
           scalefactorseth, scalefactorsmuslow, totalprodrate, percentCa, percentK, ...
           percentCl, percentCamuon, percentKmuon, computeduncertsint,radiogenicconc ] = ...
  usingcl36age( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );

%
% Get basic info about the sample ages.
%
%INDAGES=indages36(:,1)/1000;
%SIGMAAGES=indages36(:,2)/1000;
nsamples=size(nominal36,1);
%
% Update the uncertainties on the concentration.
%
% for k=1:nsamples
%   temp=cl36uncert(nominal36(k,9));
%   if (temp > uncerts36(k,9))
%     uncerts36(k,9)=temp;
%   end
% end
%
% Now, compute ages for each sample, with uncertainties.
%
computedages=zeros(nsamples,1);
computeduncerts=zeros(nsamples,1);
  scalefactorsca=zeros(nsamples,1);
  scalefactorsk=zeros(nsamples,1);
  scalefactorsti=zeros(nsamples,1);
  scalefactorsfe=zeros(nsamples,1);
  scalefactorseth=zeros(nsamples,1);
  scalefactorsth=zeros(nsamples,1);
  scalefactorsmufast=zeros(nsamples,1);
  scalefactorsmuslow=zeros(nsamples,1);
  prodrateCa=zeros(nsamples,1);
  prodrateK=zeros(nsamples,1);
  prodrateFe=zeros(nsamples,1);
  prodrateTi=zeros(nsamples,1);
  prodrateCamu=zeros(nsamples,1);
  prodrateKmu=zeros(nsamples,1);
  prodrateCl=zeros(nsamples,1);
  Sigmathss=zeros(nsamples,1);
  Sigmaethss=zeros(nsamples,1);
  Sigmascss=zeros(nsamples,1);
  Qs=zeros(nsamples,1);
  Qth=zeros(nsamples,1);
  Qeth=zeros(nsamples,1);
  Qmu=zeros(nsamples,1);
  cosmogenicconc=zeros(nsamples,1);
  radiogenicconc=zeros(nsamples,1);
  percentCa=zeros(nsamples,1);
  percentK=zeros(nsamples,1);
  percentCl=zeros(nsamples,1);
  percentCamuon=zeros(nsamples,1);
  percentKmuon=zeros(nsamples,1);
  computeduncertsint=zeros(nsamples,1);
  totalprodrate=zeros(nsamples,1);
  
for k=1:nsamples;
  tic;
  output=cl36age(nominal36(k,:),uncerts36(k,:),cov36(k),scaling_model);
  
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  scalefactorsca(k)=output(3);
  scalefactorsk(k)=output(4);
  scalefactorsti(k)=output(5);
  scalefactorsfe(k)=output(6);
  scalefactorseth(k)=output(7);
  scalefactorsth(k)=output(8);
  scalefactorsmufast(k)=output(9);
  scalefactorsmuslow(k)=output(10);
  prodrateCa(k)=output(11);
  prodrateK(k)=output(12);
  prodrateFe(k)=output(13);
  prodrateTi(k)=output(14);
  prodrateCamu(k)=output(15);
  prodrateKmu(k)=output(16);
  prodrateCl(k)=output(17);
  Sigmathss(k)=output(18);
  Sigmaethss(k)=output(19);
  Sigmascss(k)=output(20);
  Qs(k)=output(21);
  Qth(k)=output(22);
  Qeth(k)=output(23);
  Qmu(k)=output(24);
  cosmogenicconc(k)=output(25);
  radiogenicconc(k)=output(26);
  
  %output percent production using contemporary prod rates
  percentCa(k)=(output(11)+output(15))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentK(k)=(output(12)+output(16))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentCl(k)=(output(17))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentCamuon(k)=(output(15))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentKmuon(k)=(output(16))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  computeduncertsint(k)=output(28);
  
  %output total prod rate (at/g/yr)
  totalprodrate(k)=prodrateCa(k)+prodrateK(k)+prodrateFe(k)+prodrateTi(k)...
      +prodrateCamu(k)+prodrateKmu(k)+prodrateCl(k);
      
  fprintf(1,'Cl Sample %d, computed age=%f +- %f in %f minutes\n',full(...
      [k; output(1); output(2); toc/60]));
end
%
  scalefactorsti=scalefactorsti';
  scalefactorsfe=scalefactorsfe';
  scalefactorsth=scalefactorsth';

%To output everything in an "easy to paste into excel" format:
  total(:,1)=computedages';
  total(:,2)=computeduncerts'; %external uncerts
 total(:,3)=scalefactorsca';
  total(:,4)=scalefactorsk';
 total(:,5)=scalefactorseth';
  total(:,6)=scalefactorsmuslow';
  total(:,7)=totalprodrate';
  total(:,8)=percentCa';
  total(:,9)=percentK';
  total(:,10)=percentCl';
  total(:,11)=percentCamuon';
  total(:,12)=percentKmuon';
  total(:,13)=computeduncertsint';
  total(:,14)=cosmogenicconc';
  total(:,15)=radiogenicconc';

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
