function [c,P]=contourfinder(S,x,y,percentile)
%
%   c=contourfinder(S,x,y,percentile)
%
%   Uses the bisection method to find the contour level for the 
%   given percentile, on the surface S.
%
iters=0;
maxiters=20;
a=max(max(S));
b=min(min(S));
c=(a+b)/2;

while abs(a-b) > 1e-6
    A=areawithincontour(S,x,y,a)-percentile;
    B=areawithincontour(S,x,y,b)-percentile;
    C=areawithincontour(S,x,y,c)-percentile;
    if A*C < 0
        b=c;
    elseif B*C < 0
        a=c;
    end
    c=(a+b)/2;
    P=areawithincontour(S,x,y,c);
    iters=iters+1;
    
    if iters == maxiters
        break
    end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function A=areawithincontour(S,x,y,h)
for i=1:length(x)
    for j=1:length(y)
        if S(i,j)<=h;
            S(i,j)=0;
        end
    end
end
A=trapz(x,trapz(y,S,2),1);