function [erate_vs_age,age_vs_inher,erate_vs_inher]=makeplots36(...
    erates,ages,inhers,posterior_er,posterior_age,...
    posterior_inher,nominal,uncerts,depths,map,jposterior,scaling_model)
%
%   makeplots1026(erates,ages,inhers,posterior_er,posterior_age,...
%    posterior_inher,nominal,uncerts,map,jposterior,scaling_model)
%
%   Creates all 7 plots for the Be10/Al26 profile calculator output
%



% figure(1)
% clf
% plot(erates,posterior_er,'k')
% % hTitle=title('Posterior Distribution for Erosion Rate');
% hXLabel=xlabel('er (g/cm^2/kyr)');
% hYLabel=ylabel('P(er | data)');
% plotstyle(hXLabel,hYLabel)
% set(gcf,'PaperSize',[6 4.5])
% 
% figure(2)
% clf
% plot(ages,posterior_age,'k')
% % hTitle=title('Posterior Distribution for Age');
% hXLabel=xlabel('age (1000 years)');
% hYLabel=ylabel('P(age | data)');
% plotstyle(hXLabel,hYLabel)
% set(gcf,'PaperSize',[6 4.5])
% 
% figure(3)
% clf
% plot(inhers,posterior_inher,'k')
% % hTitle=title('Posterior Distribution for Inheritence');
% hXLabel=xlabel('Inheritance (years of exposure)');
% hYLabel=ylabel('P(inher | data)');
% plotstyle(hXLabel,hYLabel)
% set(gcf,'PaperSize',[6 4.5])


% Plot best fit profile against data
figure(4);
clf;
plotprof36(nominal,uncerts(:,1),depths(:,1),map(1),map(2),map(3),scaling_model);
set(gcf,'PaperSize',[6 4.5]);

% View parwise countour plots of likelihood suface
[X,Y]=meshgrid(erates,ages);
erate_vs_age=trapz(inhers,jposterior,3);
[c68 P68]=contourfinder(erate_vs_age,erates,ages,0.68);
[c95 P95]=contourfinder(erate_vs_age,erates,ages,0.95);

figure(5);
clf;
contour(X,Y,erate_vs_age',c68,'b-')
hold on
contour(X,Y,erate_vs_age',c95,'r-')
plot(map(1),map(2),'ko','MarkerFaceColor','k')
hold off
% hTitle=title('Joint Posterior Surface');
hLegend=legend('68%','95%','MAP Solution');
hXLabel=xlabel('er (g/cm^2/kyr)');
hYLabel=ylabel('age (1000 years)');
plotstyle(hXLabel,hYLabel,hLegend)
set(gcf,'PaperSize',[6 4.5]);


[X,Y]=meshgrid(ages,inhers);
age_vs_inher=trapz(erates,jposterior,1);
age_vs_inher=reshape(age_vs_inher,length(ages),length(inhers));
c68=contourfinder(age_vs_inher,ages,inhers,0.68);
c95=contourfinder(age_vs_inher,ages,inhers,0.95);

figure(6);
clf;
contour(X,Y,age_vs_inher',c68,'b-')
hold on
contour(X,Y,age_vs_inher',c95,'r-')
plot(map(2),map(3),'ko','MarkerFaceColor','k')
hold off
% hTitle=title('Joint Posterior Surface');
hLegend=legend('68%','95%','MAP Solution');
hXLabel=xlabel('age (1000 years)');
hYLabel=ylabel('Inheritance (years of exposure)');
plotstyle(hXLabel,hYLabel,hLegend)
set(gcf,'PaperSize',[6 4.5])


[X,Y]=meshgrid(erates,inhers);
erate_vs_inher=trapz(ages,jposterior,2);
erate_vs_inher=reshape(erate_vs_inher,length(erates),length(inhers));
c68=contourfinder(erate_vs_inher,erates,inhers,0.68);
c95=contourfinder(erate_vs_inher,erates,inhers,0.95);

figure(7);
clf;
contour(X,Y,erate_vs_inher',c68,'b-')
hold on
contour(X,Y,erate_vs_inher',c95,'r-')
plot(map(1),map(3),'ko','MarkerFaceColor','k')
hold off
% hTitle=title('Joint Posterior Surface');
hLegend=legend('68%','95%','MAP Solution');
hXLabel=xlabel('er (g/cm^2/kyr)');
hYLabel=ylabel('Inheritance (years of exposure)');
plotstyle(hXLabel,hYLabel,hLegend)
set(gcf,'PaperSize',[6 4.5])