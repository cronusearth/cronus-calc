% Age profile calculator script for Greenland Be-10 profile

clear
tic


% Load data
load greenland_IC06-3.mat


% Calculate depth-to-middle of the samples
depths = nominal10(:,14) + 0.5*nominal10(:,5).*nominal10(:,6);
% ndepths=length(depths);


% Define the paramter space
erates=linspace(-2,5,50)';
ages=linspace(7,13,50)';
inhers=linspace(0e3,3.5e3,50)';


% Define the prior distribution
pr=prior(erates,ages,inhers,'uniform','uniform','uniform');


% Run ageprofilecalc1026
[posterior_er,posterior_age,posterior_inher,MAP,mu_bayes,chi2grid,lhsurf,...
    jposterior]=ageprofilecalc1026(nominal10,uncerts10(:,9:10),depths(:,1),...
    erates,ages,inhers,pr);


% Display the MAP solution and Bayesian credible intervals

erCI=bayesianCI(posterior_er,erates,0.05);
ageCI=bayesianCI(posterior_age,ages,0.05);
inherCI=bayesianCI(posterior_inher,inhers,0.05);

fprintf('MAP_erate = %.2f g/cm^2/kyr \n',MAP(1))
fprintf('MAP_age   = %.2f kyr \n',MAP(2))
fprintf('MAP_inher = %.2f years of exposure \n \n',MAP(3))

% Create plots
makeplots1026(erates,ages,inhers,posterior_er,posterior_age,...
    posterior_inher,nominal10,uncerts10,depths,MAP,jposterior)

toc