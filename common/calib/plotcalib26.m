%
% First, load in the predicted concentrations.
%
load predcalibset26.mat
%
% Plot the ratios
%
figure(1);
clf;
errorbar(ageindex26,ratios,uncerts,'ko');
hold on
ylabel('N26meas/N26pred');
xlabel('site index');
plot([0 max(ageindex26)+1],[1.0 1.0],'k--');
axis([0 max(ageindex26)+1 0 2.0]);
print -depsc predcalibset26.eps
print -dpdf predcalibset26.pdf
print -dpng predcalibset26.png
