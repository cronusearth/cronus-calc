%
% This script produces .csv files corresponding to the plots
% produced by the various plotting scripts.
%
%
% calib10.m
%
% Columns are ageindex, latitude, longitude, pressure, age,
% measured 10-Be, uncert measured 10-Be, predicted 10-Be.
%
load predcalibset10.mat
m=size(nominal10,1);
A=zeros(m,8);
A(:,1)=ageindex10';
A(:,2)=nominal10(:,1);
A(:,3)=nominal10(:,2);
A(:,4)=nominal10(:,4);
A(:,5)=INDAGES;
A(:,6)=nominal10(:,9);
A(:,7)=uncerts10(:,9);
A(:,8)=predN10;
csvwrite('calib10.csv',A);
clear
%
% calib26
%
%
% Columns are ageindex, latitude, longitude, pressure, age,
% measured 26-Al, uncert measured 26-Al, predicted 26-Al.
%
load predcalibset26.mat
m=size(nominal26,1);
A=zeros(m,8);
A(:,1)=ageindex26';
A(:,2)=nominal26(:,1);
A(:,3)=nominal26(:,2);
A(:,4)=nominal26(:,4);
A(:,5)=INDAGES;
A(:,6)=nominal26(:,10);
A(:,7)=uncerts26(:,10);
A(:,8)=predN26;
csvwrite('calib26.csv',A);
clear
%
% calibset14.mat
%
%
% Columns are ageindex, latitude, longitude, pressure, age,
% measured 14-C, uncert measured 14-C, predicted 14-C.
%
load predcalibset14.mat
m=size(nominal14,1);
A=zeros(m,8);
A(:,1)=ageindex14';
A(:,2)=nominal14(:,1);
A(:,3)=nominal14(:,2);
A(:,4)=nominal14(:,4);
for k=1:m
  if (ageindex14(k)==0)
    A(k,5)=50;
  else
    A(k,5)=INDAGES(ageindex14(k));
  end
end
A(:,6)=nominal14(:,9);
A(:,7)=uncerts14(:,9);
A(:,8)=predictedN14;
csvwrite('calib14.csv',A);
clear
%
% calib3.
%
%
% Columns are ageindex, latitude, longitude, pressure, age,
% measured 3-He, uncert measured 3-He, predicted 3-He.
%
load predcalibset3.mat
m=size(nominal3,1);
A=zeros(m,8);
A(:,1)=ageindex3';
A(:,2)=nominal3(:,1);
A(:,3)=nominal3(:,2);
A(:,4)=nominal3(:,4);
for k=1:m
  A(k,5)=INDAGES(ageindex3(k));
end
A(:,6)=nominal3(:,9);
A(:,7)=uncerts3(:,9);
A(:,8)=pred3;
csvwrite('calib3.csv',A);
clear
%
% calib36
%
%
% Columns are ageindex, latitude, longitude, pressure, age,
% measured 36-Cl, predicted 36-Cl.
%
load predcalibset36.mat
m=size(nominal36,1);
A=zeros(m,8);
A(:,1)=ageindex36';
A(:,2)=nominal36(:,7);
A(:,3)=nominal36(:,8);
A(:,4)=nominal36(:,10);
for k=1:m
  A(k,5)=INDAGES(ageindex36(k));
end
A(:,6)=nominal36(:,1);
A(:,7)=uncerts36(:,1);
A(:,8)=pred36;
csvwrite('calib36.csv',A);
clear
%
% calib10second
%
% ageindex, latitude, longitude, pressure, independent age,
% calculated age.
%
load agecalib10second.mat
m=size(nominal10,1);
A=zeros(m,6);
A(:,1)=ageindex10';
A(:,2)=nominal10(:,1);
A(:,3)=nominal10(:,2);
A(:,4)=nominal10(:,4);
A(:,5)=INDAGES;
A(:,6)=computedages;
csvwrite('calib10second.csv',A);
clear
%
% calib26second
%
% ageindex, latitude, longitude, pressure, independent age,
% calculated age.
%
load agecalib26second.mat
m=size(nominal26,1);
A=zeros(m,6);
A(:,1)=ageindex26';
A(:,2)=nominal26(:,1);
A(:,3)=nominal26(:,2);
A(:,4)=nominal26(:,4);
A(:,5)=INDAGES;
A(:,6)=computedages;
csvwrite('calib26second.csv',A);
clear
%
% calib3second
%
% ageindex, latitude, longitude, pressure, independent age,
% calculated age.
%
load agecalib3second.mat
m=size(nominal3,1);
A=zeros(m,6);
A(:,1)=ageindex3';
A(:,2)=nominal3(:,1);
A(:,3)=nominal3(:,2);
A(:,4)=nominal3(:,4);
A(:,5)=INDAGES;
A(:,6)=computedages;
csvwrite('calib3second.csv',A);
clear
%
% calib36secQuant
%
% ageindex, latitude, longitude, pressure, independent age,
% calculated age.
%
load agecalib36secQuant.mat
m=size(nominal36,1);
A=zeros(m,6);
A(:,1)=ageindex36';
A(:,2)=nominal36(:,7);
A(:,3)=nominal36(:,8);
A(:,4)=nominal36(:,10);
A(:,5)=INDAGES;
A(:,6)=computedages;
csvwrite('calib36secQuant.csv',A);
clear
%
% calib36secQual
%
% ageindex, latitude, longitude, pressure, independent age,
% calculated age.
%
load agecalib36secQual.mat
m=size(nominal36,1);
A=zeros(m,6);
A(:,1)=ageindex36';
A(:,2)=nominal36(:,7);
A(:,3)=nominal36(:,8);
A(:,4)=nominal36(:,10);
A(:,5)=INDAGES;
A(:,6)=computedages;
csvwrite('calib36secQual.csv',A);
clear