%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calibset26.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages26(ageindex26,1)/1000;
SIGMAAGES=indages26(ageindex26,2)/1000;
nsamples=length(INDAGES);
%
% Update the uncertainties on the 26-Al concentration.
%
for k=1:nsamples
  temp=al26uncert(nominal26(k,10));
  if (temp > uncerts26(k,10))
    uncerts26(k,10)=temp;
  end
end
%
% Now, compute predicted N26 for each sample, and compute ratios.
%
measuredN26=nominal26(:,10);
predN26=zeros(size(measuredN26));
ratios=zeros(size(measuredN26));
for k=1:nsamples;
  [pp,sp,sf,cp]=getpars1026(nominal26(k,:));
  [unused10,predN26(k)]=predN1026(pp,sp,sf,cp,INDAGES(k));
  ratios(k)=measuredN26(k)/predN26(k);
  uncerts(k)=uncerts26(k,10)/predN26(k);
  fprintf(1,'Sample %d, N26pred=%f, N26meas=%f, ratio=%f +- %f\n',full(...
      [k; predN26(k); measuredN26(k); ratios(k); uncerts(k)]));
end
%
% Compute the errors.
%
percenterrors=100*(ratios-ones(size(ratios)));
RMSE=sqrt(mean(percenterrors.^2))
%
% Save the results.
%
save predcalibset26.mat
quit
