function [sigmapr,sigmaia,sigmaint,rmse,total]=ProdRateUncertCalc(scaling_model,inputfilename,outputfilename)

%This function calculates the production rate uncertainty based on a full
%dataset of 36Cl samples. This does not separate out Cl into groups by Cl
%content; instead, you must run this code once for low-Cl samples and once
%for high-Cl samples in order to obtain production rate uncertainties on
%both spallation and Pf0 parameters. Most of this code can be reused to
%produce a similar code for Be, Al, etc. 

%[sigmapr,total]=ProdRateUncertCalc(scaling_model,inputfilename,outputfilename)

tic
%load file for dataset
load( inputfilename, '-mat' );

%calculate ages for each sample using final production rates
%(taken from usingcl36age)

nsamples=size(nominal36,1);
computedages=zeros(nsamples,1);
for k=1:nsamples;
  
  output=cl36age(nominal36(k,:),uncerts36(k,:),cov36(k),scaling_model);
  
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
%   scalefactorsca(k)=output(3);
%   scalefactorsk(k)=output(4);
%   scalefactorsti(k)=output(5);
%   scalefactorsfe(k)=output(6);
%   scalefactorseth(k)=output(7);
%   scalefactorsth(k)=output(8);
%   scalefactorsmufast(k)=output(9);
%   scalefactorsmuslow(k)=output(10);
  prodrateCa(k)=output(11);
  prodrateK(k)=output(12);
  prodrateFe(k)=output(13);
  prodrateTi(k)=output(14);
  prodrateCamu(k)=output(15);
  prodrateKmu(k)=output(16);
  prodrateCl(k)=output(17);
  Sigmathss(k)=output(18);
  Sigmaethss(k)=output(19);
  Sigmascss(k)=output(20);
%   Qs(k)=output(21);
%   Qth(k)=output(22);
%   Qeth(k)=output(23);
%   Qmu(k)=output(24);
%   cosmogenicconc(k)=output(25);
%   radiogenicconc(k)=output(26);
  %output percent production using contemporary prod rates
  percentCa(k)=(output(11)+output(15))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentK(k)=(output(12)+output(16))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentCl(k)=(output(17))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentCamuon(k)=(output(15))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  percentKmuon(k)=(output(16))/(output(11)+output(12)+output(13)+...
  output(14)+output(15)+output(16)+output(17))*100;
  computeduncertsint(k)=output(28);
  
  %output total prod rate (at/g/yr)
  totalprodrate(k)=prodrateCa(k)+prodrateK(k)+prodrateFe(k)+prodrateTi(k)...
      +prodrateCamu(k)+prodrateKmu(k)+prodrateCl(k);
  %calculate normalized age, diff, squared
  % convert to normalized ages by dividing the calculated age by indpt age
  normage(k)=output(1)/(indages36(k,1)/1000);
  %normalize the uncertainty as well
  relintunc(k)=computeduncertsint(k)/(indages36(k,1)/1000);
  diff(k)=(1-normage(k))^2;
  fprintf(1,'Cl Sample %d, computed age=%f +- %f in %f minutes\n',full(...
      [k; output(1); output(2); toc/60]));
end
  total(:,1)=computedages';
  total(:,2)=computeduncertsint'; %internal uncerts
  total(:,3)=normage';
  total(:,4)=diff';
  total(:,5)=percentCa';
  total(:,6)=percentK';
  total(:,7)=percentCl';
  total(:,8)=percentCamuon';
  total(:,9)=percentKmuon';

%Calculate RMSE
%RMSE is the observed variability  
rmse=sqrt(mean(diff));

%independent age uncert calculated from the radiocarbon bounds and their
%uncertainties
relindage=indages36(:,2)./indages36(:,1);
sigmaia=mean(relindage);

%average of internal uncertainties
sigmaint=mean(relintunc);

%use the eqn in the paper to solve for prod rate uncertainty
sigmapr=sqrt(rmse^2-sigmaia^2-sigmaint^2);

%report uncertainty and values for other parts of the table
%toc/60
fprintf('Scaling model: %s \n',[scaling_model]);
fprintf(1,'Production rate uncertainty is %f \n',full([sigmapr]));
fprintf(1,'Total RMSE is %f \n',full([rmse]));
fprintf(1,'Uncertainty from independent age is %f \n',full([sigmaia]));
fprintf(1,'Uncertainty from internal uncerts is %f \n',full([sigmaint]));
 save( outputfilename );