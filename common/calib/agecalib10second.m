%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calib10second.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages10(:,1)/1000;
SIGMAAGES=indages10(:,2)/1000;
nsamples=length(INDAGES);
%
% Update the uncertainties on the 10-Be concentration.
%
for k=1:nsamples
  temp=be10uncert(nominal10(k,9));
  if (temp > uncerts10(k,9))
    uncerts10(k,9)=temp;
  end
end
%
% Now, compute ages for each sample.
%
computedages=zeros(nsamples,1);
for k=1:nsamples;
  output=be10age(nominal10(k,:),uncerts10(k,:));
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  scalefactorsn(k)=output(3);
  scalefactorsmu(k)=output(4);
  fprintf(1,'%4d ',k);
  fprintf(1,'%-15.15s ',samplenames{k});
  fprintf(1,' indage=%f +- %f, computed age=%f +- %f \n',full(...
      [INDAGES(k); SIGMAAGES(k); output(1); output(2)]));
end
%
% Compute the errors.
%
percenterrors=100*(computedages-INDAGES)./INDAGES;
RMSE=sqrt(mean(percenterrors.^2))
%
% Compute mean site ages.  
%
nsites=max(ageindex10);
meansiteages=zeros(nsites,1);
mediansiteages=zeros(nsites,1);
for k=1:nsites
  range=(ageindex10 == k);
  meansiteages(k)=mean(computedages(range));
  mediansiteages(k)=median(computedages(range));
end
meansiteerrors=100*(meansiteages*1000-indagenew10(:,1))./indagenew10(:,1);
meansiteRMSE=sqrt(mean(meansiteerrors.^2));
mediansiteerrors=100*(mediansiteages*1000-indagenew10(:,1))./indagenew10(:,1);
mediansiteRMSE=sqrt(mean(mediansiteerrors.^2));
%
% Compute ratios of ages/INDAGES
%
ratios=computedages./INDAGES;
%
% Print out a table of results.
%
fprintf(1,'\n');
for k=1:nsites
  fprintf(1,'%-4.4s ',sitenames{k});
  fprintf(1,'%5.2f +- %4.2f %4.2f%%   ',...
	  [indagenew10(k,1)/1000; indagenew10(k,2)/1000; ...
	  100*indagenew10(k,2)/indagenew10(k,1)]);
  fprintf(1,'%5.2f  %5.2f%%    ',[meansiteages(k); meansiteerrors(k)]);
  fprintf(1,'%5.2f  %5.2f%% ',[mediansiteages(k); ...
		    mediansiteerrors(k)]);
  fprintf(1','\n');
end


%
% Save the results.
%
save agecalib10second.mat
quit
