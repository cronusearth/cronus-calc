%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calib3second.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages3(:,1)/1000;
SIGMAAGES=indages3(:,2)/1000;
nsamples=length(INDAGES);
%
% Update the uncertainties on the 10-Be concentration.
%
for k=1:nsamples
%  temp=be10uncert(nominal10(k,9));
%  if (temp > uncerts10(k,9))
%    uncerts10(k,9)=temp;
%  end
  SIGMACONC(k)=uncerts3(k,9);
end
%
% Now, compute ages for each sample, with uncertainties.
%
computedages=zeros(nsamples,1);
for k=1:nsamples;
  output=he3age(nominal3(k,:),uncerts3(k,:));
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  scalefactorsn(k)=output(3);
  scalefactorsmu(k)=output(4);
  fprintf(1,'%4d ',k);
  fprintf(1,'%-15.15s ',samplenames{k});
  fprintf(1,' indage=%f +- %f, computed age=%f +- %f \n',full(...
      [INDAGES(k); SIGMAAGES(k); output(1); output(2)]));
end
%
% Compute ratios of ages/INDAGES
%
ratios=computedages./INDAGES;
%
% Compute the errors.
%
percenterrors=100*(computedages-INDAGES)./INDAGES;
RMSE=sqrt(mean(percenterrors.^2))

%
% Compute mean site ages.  
%
nsites=max(ageindex3);
meansiteages=zeros(nsites,1);
mediansiteages=zeros(nsites,1);
for k=1:nsites
  range=(ageindex3 == k);
  meansiteages(k)=mean(computedages(range));
  mediansiteages(k)=median(computedages(range));
end
meansiteerrors=100*(meansiteages*1000-indagenew3(:,1))./ ...
    indagenew3(:,1);
meansiteRMSE=sqrt(mean(meansiteerrors.^2));
mediansiteerrors=100*(mediansiteages*1000-indagenew3(:,1))./ ...
    indagenew3(:,1);
mediansiteRMSE=sqrt(mean(mediansiteerrors.^2));
%
% Print out a table of results.
%
fprintf(1,'\n');
for k=1:nsites
  fprintf(1,'%-6.6s ',sitenames{k});
  fprintf(1,'%5.2f +- %4.2f %4.2f%%   ',...
          [indagenew3(k,1)/1000; indagenew3(k,2)/1000; ...
          100*indagenew3(k,2)/indagenew3(k,1)]);
  fprintf(1,'%5.2f  %5.2f%%    ',[meansiteages(k); ...
		    meansiteerrors(k)]);
  fprintf(1,'%5.2f  %5.2f%% ',[mediansiteages(k); ...
                    mediansiteerrors(k)]);
  fprintf(1','\n');
end


%
% Save the results.
%
save agecalib3second.mat
quit
