%
% This script plots the computed ages for calibset10.
%
load agecalibset26second.mat
%
% First, setup ranges that define which samples are from which
% sites in the 10-Be data set.
%
%  1. NEBH
%  2. W86
%  3. WY
%  4. JSS
%
rangenebh=1:14;
rangew86=15:24;
rangewy=25:30;
rangejss=31:32;
%
% Set up a mapping from sample number to site number.
%
sites=zeros(32,1);
sites(rangenebh)=1;
sites(rangew86)=2;
sites(rangewy)=3;
sites(rangejss)=4;
%
% Now, get independent ages for the sites.
%
siteages=[INDAGES(1); INDAGES(15); INDAGES(25); INDAGES(31)];
siteuncerts=[SIGMAAGES(1); SIGMAAGES(15); SIGMAAGES(25); SIGMAAGES(31)];
%
% Plot the results.
%
figure(4);
clf;
errorbar(sites-0.03*mod((1:32)',10),computedages,computeduncerts,'ko');
hold on
errorbar(1.1:4.1,siteages,siteuncerts,'ro');
axis([0 5 0 25]);
xlabel('Site (1=NEBH, 2=W86, 3=WY, 4=JSS)');
ylabel('Age (kyr before 2010)');
print -dpng plotcalibset26secondages.png
print -depsc plotcalibset26secondages.eps
%
% Compute % errors for all samples and the RMSE.
%
percenterrors=zeros(32,1);
for k=1:32
  percenterrors(k)=100*(computedages(k)/INDAGES(k)-1);
end
rmse=sqrt(mean(percenterrors.^2));
fprintf(1,'RMSE=%f%% \n',rmse);
%
% Print out some results.
%
fprintf(1,['1, NEBH, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(1); siteuncerts(1); mean(computedages(rangenebh)); ...
	 std(computedages(rangenebh)); ... 
	 100*(mean(computedages(rangenebh))/siteages(1)-1)]);
fprintf(1,['2, W86, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(2); siteuncerts(2); mean(computedages(rangew86)); ...
	 std(computedages(rangew86)); ... 
	 100*(mean(computedages(rangew86))/siteages(2)-1)]);
fprintf(1,['3, WY , ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(3); siteuncerts(3); mean(computedages(rangewy)); ...
	 std(computedages(rangewy)); ... 
	 100*(mean(computedages(rangewy))/siteages(3)-1)]);
fprintf(1,['4, JSS, ind=%.2f +- %.2f, mean comp=' ...
	   '%.2f, std comp age=%.2f, err=%.2f%% \n'], ...
	[siteages(4); siteuncerts(4); mean(computedages(rangejss)); ...
	 std(computedages(rangejss)); ... 
	 100*(mean(computedages(rangejss))/siteages(4)-1)]);
