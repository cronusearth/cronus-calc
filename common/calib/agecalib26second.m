%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calib26second.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages26(:,1)/1000;
SIGMAAGES=indages26(:,2)/1000;
nsamples=length(INDAGES);
%
% Update the uncertainties on the 26-Al concentration.
%
for k=1:nsamples
  temp=al26uncert(nominal26(k,10));
  if (temp > uncerts26(k,10))
    uncerts26(k,10)=temp;
  end
end
%
% Now, compute ages for each sample, with uncertainties.
%
computedages=zeros(nsamples,1);
for k=1:nsamples;
  output=al26age(nominal26(k,:),uncerts26(k,:));
  computedages(k)=output(1);
  computeduncerts(k)=output(2);
  scalefactorsn(k)=output(3);
  scalefactorsmu(k)=output(4);
  fprintf(1,'%4d ',k);
  fprintf(1,'%-15.15s ',samplenames{k});
  fprintf(1,' indage=%f +- %f, computed age=%f +- %f \n',full(...
      [INDAGES(k); SIGMAAGES(k); output(1); output(2)]));
end
%
% Compute ratios of computedages./INDAGES
%
ratios=computedages./INDAGES;
%
% Compute the errors.
%
percenterrors=100*(computedages-INDAGES)./INDAGES;
RMSE=sqrt(mean(percenterrors.^2))

%
% Compute mean site ages.  
%
nsites=max(ageindex26);
meansiteages=zeros(nsites,1);
mediansiteages=zeros(nsites,1);
for k=1:nsites
  range=(ageindex26 == k);
  meansiteages(k)=mean(computedages(range));
  mediansiteages(k)=median(computedages(range));
end
meansiteerrors=100*(meansiteages*1000-indagenew26(:,1))./indagenew26(:,1);
meansiteRMSE=sqrt(mean(meansiteerrors.^2));
mediansiteerrors=100*(mediansiteages*1000-indagenew26(:,1))./indagenew26(:,1);
mediansiteRMSE=sqrt(mean(mediansiteerrors.^2));
%
% Print out a table of results.
%
fprintf(1,'\n');
for k=1:nsites
  fprintf(1,'%-4.4s ',sitenames{k});
  fprintf(1,'%5.2f +- %4.2f %4.2f%%   ',...
	  [indagenew26(k,1)/1000; indagenew26(k,2)/1000; ...
	  100*indagenew26(k,2)/indagenew26(k,1)]);
  fprintf(1,'%5.2f  %5.2f%%    ',[meansiteages(k); meansiteerrors(k)]);
  fprintf(1,'%5.2f  %5.2f%% ',[mediansiteages(k); ...
		    mediansiteerrors(k)]);
  fprintf(1','\n');
end

%
% Save the results.
%
save agecalib26second.mat
quit
