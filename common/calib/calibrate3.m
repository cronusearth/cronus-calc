%
% This script runs the calibration of production rates from the
% Calibration Data Set.  This version of the script fits 
%
%  PsHe
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.  The original dataset had some problems (0
% standard deviations.)  These have been temporarily patched up.
%
load calibset3.mat
%
% global variables to hold stuff that must be visible to fun and jac.
%
global INDAGES;
global SIGMAAGES;
global SAMPLES;
global STOREDSF;
global STOREDSP;
global STOREDCP;
global CONC;
global SIGMACONC;
global AGEINDEX;
%
% Store the dataset into global variables that fun and jac can access.
% Note that we convert the independent ages into the kyrs units
% that we prefer in our code.
%
SAMPLES=nominal3;
%
%
%
INDAGES=indagenew3(:,1)/1000;
SIGMAAGES=indagenew3(:,2)/1000;
AGEINDEX=ageindex3;
%
% Figure out the number of samples.
%
[nsamples,n2]=size(SAMPLES);
[nages,n2]=size(INDAGES);
%
% We're fitting 1 parameter, plus the nsamples deltaage parameters.
%
npars=1;
%
% Get the sample concentrations and uncertainties.
%
CONC=SAMPLES(:,9);
%
% Use uncertainties from the laboratory intercomparison, or the
% supplied uncertainty, whichever is larger.   
%

for k=1:nsamples
  temp=he3uncert(CONC(k));
  if (temp > uncerts3(k,9))
    SIGMACONC(k)=temp;
  else
    SIGMACONC(k)=uncerts3(k,9);
  end
end

%
% Precompute scale factors for the various samples.
%
for k=1:nsamples;
  sp=samppars3(SAMPLES(k,:));
  pp=physpars();
  sf=scalefacs3(sp);
  cp=comppars3(pp,sp,sf);
  sf.Sel=getcurrentsf(sf,0);
  STOREDSF{k}=sf;
  STOREDSP{k}=sp;
  STOREDCP{k}=cp;
end
%
% Setup an initial estimate of PsHe.  We'll use 130.0.
%
pinit=[130.0; zeros(nages,1)];
%
% Pick a maximum number of iterations for LM.
%
maxiter=100;
%
% Call LM to do the fitting.
%
[pstar,iter]=lm('odrfun3','odrjac3',pinit,1.0e-4,maxiter);
%
% Check whether we got good termination or just reached maxiter.
%
if (iter >= maxiter)
  fprintf(1,'Reached maximum LM iterations of %d\n',maxiter);
end
%
% Find the covariance matrix for the fitted parameters.
%
rstar=odrfun3(pstar);
Jstar=odrjac3(pstar);
%
% Compute a p-value for the fit.
%
chi2=norm(rstar,2)^2
pvalue=1-chi2cdf(chi2,nsamples-npars)
%
% Compute a covariance matrix for the fit.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
%
% Print out the result.
%
fprintf(1,'PsHe= %f +- %f \n',[pstar(1); sigmapstar(1)]);
save calibrate3.mat
quit
