function [finalpf0, pf0TAB, pf0BL] = calibrate36pf0avg(scaling_model)
% This script runs both Pf0 calibration data sets (TAB and BL) and takes
% the average of the two values to produce Pf0. 
%tic
[pstarTAB, sigmapstarTAB, chi2TAB, pvalueTAB]=calibrate36pf0TAB(scaling_model);
[pstarBL, sigmapstarBL, chi2BL, pvalueBL]=calibrate36pf0BL(scaling_model);

pf0TAB = pstarTAB(1);
pf0BL = pstarBL(1);
finalpf0=(pf0TAB+pf0BL)/2;
fprintf('Scaling model: %s \n',[scaling_model]);
fprintf(1,'TAB: Pf(0)= %f (Chi^2=%f, pvalue=%f)\n',[pstarTAB(1); chi2TAB; pvalueTAB]);
fprintf(1,'BL: Pf(0)= %f (Chi^2=%f, pvalue=%f)\n',[pstarBL(1); chi2BL; pvalueBL]);
fprintf(1,'Pf(0)= %f\n',[finalpf0]);
%toc