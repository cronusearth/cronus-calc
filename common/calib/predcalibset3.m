%
% This script computes ages for the Calibration samples using
% standard production rate. 
%
% Reset everything first.
%
clear
randn('state',0);
rand('state',0);
%
% load in the data.
%
load calibset3.mat
%
% Get basic info about the sample ages.
%
INDAGES=indages3(:,1)/1000;
SIGMAAGES=indages3(:,2)/1000;
[nsamples,ignore]=size(nominal3);
%
% Now, compute predicted N3 for each sample, and compute ratios.
%
measuredN3=nominal3(:,9);
pred3=zeros(size(measuredN3));
ratios=zeros(size(measuredN3));
uncerts=ratios;

for k=1:nsamples;
  [pp,sp,sf,cp]=getpars3(nominal3(k,:));
  pred3(k)=predN3(pp,sp,sf,cp,INDAGES(ageindex3(k)));
  ratios(k)=measuredN3(k)/pred3(k);
  temp=he3uncert(nominal3(k,9));
  if (temp > uncerts3(k,9))
    uncerts(k)=temp;
  else
    uncerts(k)=uncerts3(k,9);
  end
  uncerts(k)=uncerts(k)/pred3(k);
  fprintf(1,'Sample %d, N3pred=%f, N3meas=%f, ratio=%f +- %f \n',full(...
      [k; pred3(k); measuredN3(k); ratios(k); uncerts(k)]));
end
%
% Compute the errors.
%
percenterrors=100*(ratios-ones(size(ratios)));
RMSE=sqrt(mean(percenterrors.^2))
%
% Save the results.
%
save predcalibset3.mat
quit
