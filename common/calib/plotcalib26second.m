%
% First, load in the predicted concentrations.
%
load agecalib26second.mat
%
% Plot the ratios
%
figure(1);
clf;
plot(ageindex26,ratios,'ko');
hold on
ylabel('Computed Age/Independent Age');
xlabel('site index');
plot([0 max(ageindex26)+1],[1.0 1.0],'k--');
axis([0 max(ageindex26)+1 0 2.0]);
print -depsc agecalibset26second.eps
print -dpdf agecalibset26second.pdf
print -dpng agecalibset26second.png
