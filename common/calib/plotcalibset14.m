%
% First, load in the predicted concentrations.
%
load predcalibset14.mat
%
% Plot the ratios
%
figure(1);
clf;
errorbar(siteindex14,ratios,uncerts,'ko');
hold on
ylabel('N14meas/N14pred');
xlabel('site index');
plot([0 max(siteindex14)+1],[1.0 1.0],'k--');
axis([0 max(siteindex14)+1 0 2.0]);
print -depsc predcalibset14.eps
print -dpdf predcalibset14.pdf
print -dpng predcalibset14.png
