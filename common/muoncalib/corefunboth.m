function r=corefunboth(p)
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% Deal with the case the erosion rate is negative.
%
if (p<0)
  r=1.0e30*ones(nsamples,1);
  return
end
%
% Pick out the first sample for lat/lon etc.
%
sample=samples(1,:);
%
% Put parameter 1 into the erosion rate.
%
sample(8)=p(1);
%
% Put parameter 2 into the attenuation length.
%
sample(13)=p(2);
%
% Set a maximum depth.
%
maxdepth=30000;
%
% Get the rest of the parameters.
%
pp=physpars();
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
r=zeros(nsamples*2,1);
[N10,foo]=predN1026depth(pp,sp,sf,cp,age,depths);
for i=1:nsamples
  r(i)=(N10(i)-samples(i,9))/E(i);
end
%
% Now, use a different attenuation length for 26-Al.
%
%
% Put parameter 2 into the attenuation length.
%
sample(13)=p(3);
%
% Set a maximum depth.
%
maxdepth=20000;
%
% Get the rest of the parameters.
%
pp=physpars();
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
[foo,N26]=predN1026depth(pp,sp,sf,cp,age,depths);
for i=1:nsamples
  if (samples(i,10) > 0)
    r(i+nsamples)=(N26(i)-samples(i,10))/E(i+nsamples);
  else
    r(i+nsamples)=0;
  end
end
