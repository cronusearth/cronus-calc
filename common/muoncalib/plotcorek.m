%
% This script plots the results from calbemuons.m
%
%
% Load in the .mat file containing the results.
%
load COREK36B.mat
%
% Get the number of samples.
%
[nsamples,thirtyeight]=size(nominal36);
%
% Figure out midpoint depths.
%
depths=zeros(nsamples,1);
for k=1:nsamples
  depths(k)=nominal36(k,37)+0.5*nominal36(k,5)*nominal36(k,6);
end
%
% Set the erosion rate to 8.5mm/kyr.
%
nominal36(:,3)=8.5;
%
% Compute predicted 36-Cl using the fitted parameters.
%
maxdepth=30000;
age=10000;
pp=physpars();

N36pred=zeros(size(depths));
for k=1:nsamples
  sp=samppars36(nominal36(k,:));
  sf=scalefacs36(sp);
  cp=comppars36(pp,sp,sf,maxdepth);
  N36pred(k)=predN36depth(pp,sp,sf,cp,age,depths(k));
end
%
% Plot the 36-Cl profile.
%
figure(1);
clf;
herrorbar(nominal36(:,1),depths,uncerts36(:,1),'ko');
set(gca,'YDir','reverse');
axis([0 2500000 0 2000]); 
hold on
plot(N36pred,depths,'k.');
xlabel('36-Cl Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng plotcorek.png
print -dpdf plotcorek.pdf
