function r=corefun1026(p)
%
% This version is for simultaneous calibration of the 10-Be and
% 26-Al attenuation lengths, the erosion rate, and the muon
% production parameters for 10-Be and 26-Al.  
%
% The parameters are:
%   p(1)             erosion rate
%   p(2)             attenuation length for 10-Be
%   p(3)             attenuation length for 26-Al
%   p(4)             fstar10 (scaled by 1.0e-3)
%   p(5)             sigma010 (scaled by 1.0e-30)
%   p(6)             fstar26 (scaled by 1.0e-3)
%   p(7)             sigma026 (scaled by 1.0e-30)
%
% Global parameters.
%
global samples;
global depths;
global E;
global nsamples10;
global nsamples26;
global nresids;
global npars;
%
% Deal with the case that any parameter is negative.
%
if any(p<0)
  r=1.0e30*ones(nresids,1);
  return
end
%
% Pick out the first sample for lat/lon etc.
%
sample=samples(1,:);
%
% Put parameter 1 into the erosion rate.
%
sample(8)=p(1);
%
% Put parameter 2 into the attenuation length for 10-Be.  
%
sample(13)=p(2);
%
% Set a maximum depth.
%
maxdepth=30000;
%
% Get the rest of the parameters.
%
pp=physpars();
%
% Set the muon parameters.
%
pp.fstar10=p(4)*1.0e-3;
pp.sigma010=p(5)*1.0e-30;
pp.fstar26=p(6)*1.0e-3;
pp.sigma026=p(7)*1.0e-30;
% 
% Compute all of the other parameters.
%
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
r=zeros(nresids,1);
[N10,foo]=predN1026depth(pp,sp,sf,cp,age,depths);
for i=1:nsamples10
  r(i)=(N10(i)-samples(i,9))/E(i);
end
%
% Now, use a different attenuation length for 26-Al.
%
%
% Put parameter 3 into the attenuation length.
%
sample(13)=p(3);
%
% Set a maximum depth.
%
maxdepth=20000;
%
% Get the rest of the parameters.
%
pp=physpars();
%
% Set the muon parameters for 26-Al.
%
pp.fstar10=p(4)*1.0e-3;
pp.sigma010=p(5)*1.0e-30;
pp.fstar26=p(6)*1.0e-3;
pp.sigma026=p(7)*1.0e-30;
%
% Figure out the rest of the parameters.
%
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
[foo,N26]=predN1026depth(pp,sp,sf,cp,age,depths);
k=1;
for i=1:nsamples10
  if (samples(i,10) > 0)
    r(i+k)=(N26(i)-samples(i,10))/E(i+nsamples10);
    k=k+1;
  end
end
