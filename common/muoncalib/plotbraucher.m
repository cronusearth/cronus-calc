%
% This script plots the results from calbemuons.m
%
%
% Load in the .mat file containing the results.
%
load BHC1026BrauchA.mat
[nsamples,fifteen]=size(nominal10);
%
% Compute predicted 10-Be using the fitted parameters.
%
maxdepth=20000;
pp=physpars();
sp=samppars1026(nominal10(1,:));
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We need a range of depths from 0 to 6500 g/cm^2.  
%
plotteddepths=([0:50:1000 1250:250:6500])';
%
% We're using an age of 10 million years (long enough to reach saturation.)
%
age=10000;
%
% Compute the predicted concentrations.
%
[predictedN10,predictedN26]=predN1026depth(pp,sp,sf,cp,age,plotteddepths);
%
% Uncertainties on Braucher data.
%
for k=1:nsamples,
  E(k)=be10uncert(nominal10(k,9));
  Eal(k)=al26uncert(nominal10(k,10));
end
%
% Predictions for Braucher data.
%
braucherdepths=nominal10(:,14)+0.5*nominal10(:,5).*nominal10(:,6);
[braucherN10,braucherN26]=predN1026depth(pp,sp,sf,cp,age,braucherdepths);
%
% Note that in the following you'll need a copy of herrorbar from
% the MATLAB file-exchange.  
%
%
% Plot the 10-Be profile.
%
figure(1);
clf;
herrorbar(nominal10(:,9),braucherdepths,E(1:nsamples),'ko');
set(gca,'YDir','reverse');
axis([0 7e7 0 1000]); 
hold on
plot(predictedN10,plotteddepths,'k');
xlabel('10-Be Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng plotbraucher1.png
print -depsc plotbraucher.eps

figure(2);
clf;
herrorbar(nominal10(:,9),braucherdepths,E(1:nsamples),'ko');
set(gca,'YDir','reverse');
axis([0 200000 0 6500]); 
hold on
plot(predictedN10,plotteddepths,'k');
xlabel('10-Be Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng plotbraucher2.png
print -depsc plotbraucher2.eps

figure(3);
clf;
herrorbar(nominal10(:,10),braucherdepths,Eal(1:nsamples),'ko');
set(gca,'YDir','reverse');
axis([0 3e8 0 1000]); 
hold on
plot(predictedN26,plotteddepths,'k');
xlabel('26-Al Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng plotbraucher3.png
print -depsc plotbraucher3.eps

figure(4);
clf;
herrorbar(nominal10(:,10),braucherdepths,Eal(1:nsamples),'ko');
set(gca,'YDir','reverse');
axis([0 3e6 0 6500]); 
hold on
plot(predictedN26,plotteddepths,'k');
xlabel('26-Al Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng plotbraucher4.png
print -depsc plotbraucher4.eps
