function [out_coreCa]=calibratemuonCa(scaling_model)
% This script uses a depth profile to calibrate the
% muon production parameters for production of 36-Cl from Ca.

% out_coreCa is composed of:
% fstarCa and sigma0Ca - the calibrated muon parameters. Uncertainties are
% also included for these parameters (sigmafstarCa and sigmasigma0Ca)
% Additional parameters are also fitted to the dataset: 
% erate_coreCa - calculated erosion rate
% overburdenCa - the additional overburden that was removed off the top of the profile
% attenlength_coreCa - the calculated attenuation length as calculated from the dataset
% Chi^2 and p-values are reported in out as well. 

%
% Load in the data.
%
load calib36coreCa.mat
%
% Global variables.
%
global E;
global samples;
global depths;
global nsurfacesamples;
%
% Note that the first three samples are true surface samples.
%
nsurfacesamples=1;
%
% Figure out depths to middle of samples.
%
samples=nominal36;
depths=nominal36(:,37)+0.5*nominal36(:,5).*nominal36(:,6);
%
% Get the number of samples.
%
[nsamples,thirtyeight]=size(samples);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples
   E(k)=uncerts36(k,1);
   if (cl36uncert(nominal36(k,1))> E(k))
     E(k)=cl36uncert(nominal36(k,1));
   end
end
%
% An initial guess at the parameters.
%
npars=5;
pinit=[6.0; 160; 150; 1.0; 7.0];
%
% Use LM to find optimal values of parameters.
%
[pstar,iter]=lm('corefun36Ca','corejac36Ca',pinit,1.0e-5,100,scaling_model);
%
% Compute the residual and J at the optimal parameters.
%
rstar=corefun36Ca(pstar,scaling_model);
Jstar=corejac36Ca(pstar,scaling_model);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nsamples-npars);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf('Scaling model: %s \n',[scaling_model]);
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'erosion rate=%f +- %f \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'attenuation length=%f +- %f  \n',[pstar(2); ...
		    sigmapstar(2)]);
fprintf(1,'additional overburden=%f +- %f  \n',[pstar(3); ...
		    sigmapstar(3)]);
fprintf(1,'fstarCa=%f +- %f \n',[pstar(4); ...
		    sigmapstar(4)]);
fprintf(1,'sigma0Ca=%f +- %f  \n',[pstar(5); ...
		    sigmapstar(5)]);
save calClCaprofileresults1.mat

out_coreCa.fstarCa = pstar(4);
out_coreCa.sigmafstarCa = sigmapstar(4);
out_coreCa.sigma0Ca = pstar(5);
out_coreCa.sigmasigma0Ca = sigmapstar(5);
out_coreCa.erate_coreCa = pstar(1);
out_coreCa.sigmaerate_coreCa = sigmapstar(1);
out_coreCa.overburdenCa = pstar(3);
out_coreCa.sigmaoverbudenCa = sigmapstar(3);
out_coreCa.attenlength_coreCa = pstar(2);
out_coreCa.sigmaattenlength_coreCa = sigmapstar(2);
out_coreCa.chi2 = chi2;
out_coreCa.pvalue = pvalue;

for i=1:npars
  for j=1:npars
    corp(i,j)=covp(i,j)/(sqrt(covp(i,i))*sqrt(covp(j,j)));
  end
end
corp;
%
% Save the results.
%
save calClCaprofileresults.mat
