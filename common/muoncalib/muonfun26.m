function r=muonfun26(p)
%
% Global parameters.
%
global samples;
global depths;
global E;
%
% Get the number of samples.
%
[nsamples,foo]=size(samples);
%
% Deal with the case that parameters are negative.
%
if (p(1)<0)
  r=1.0e30*ones(nsamples,1);
  return
end
if (p(2)<0)
  r=1.0e30*ones(nsamples,1);
  return
end

%
% Pick out the first sample for lat/lon etc.
%
sample=samples(1,:);
%
% Set a maximum depth.
%
maxdepth=20000;
%
% Get the rest of the parameters.
%
pp=physpars();
pp.sigma026=p(1)*1.0e-30;
pp.fstar26=p(2)*1.0e-3;
sp=samppars1026(sample);
sf=scalefacs1026(sp);
cp=comppars1026(pp,sp,sf,maxdepth);
%
% We'll use an age of 10 million years.
%
age=10000;
%
% Loop through the samples and compute predicted concentrations and residuals. 
%
r=zeros(nsamples,1);
[N10,N26]=predN1026depth(pp,sp,sf,cp,age,depths);
for i=1:nsamples
  r(i)=(N26(i)-samples(i,10))/E(i);
end
