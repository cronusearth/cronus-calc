%
% This script uses the Beacon Hill 10-Be profile to calibrate the
% attenuation length and erosion rate for 10-Be production at this site. 
%
% Load in the data.
%
load BHC1026StoneB.mat
%
% Global variables.
%
global E;
global samples;
global depths;
%
% Take only the top most samples for this purpose.
%
range=1:16;
samples=nominal10(range,:);
%
% Get the number of samples.
%
[nsamples,fifteen]=size(samples);
%
% Figure out how many samples actually have Aluminum.
%
nalsamples=sum(samples(:,10)~=0);
%
% Compute uncertainties on the concentrations.
%
for k=1:nsamples
  E(k)=be10uncert(samples(k,9));
end
%
% Note that some samples have no measured 26-Al, so we'll set those
% residuals to 0 in computing Chi^2.
%
for k=1:nsamples
  if (samples(k,10) > 0)
    E(k+nsamples)=al26uncert(samples(k,10));
  else
    E(k+nsamples)=1;
  end
end
%
% Separate out the depths.
%
depths=samples(:,14)+0.5*samples(:,5).*samples(:,6);
%
% An initial guess at the erosion rate and attenuation lengths.
%
pinit=[0.02; 140.0; 140.0];
%
% Use LM to find optimal values of erosion rate and attenuation length.
%
[pstar,iter]=lm('corefunboth','corejacboth',pinit,1.0e-4,100);
%
% Compute the residual and J at the optimal parameters.
%
rstar=corefunboth(pstar);
Jstar=corejacboth(pstar);
%
% Compute Chi2 and pvalue.
%
chi2=norm(rstar,2)^2;
pvalue=1-chi2cdf(chi2,nsamples+nalsamples-3);
%
% Compute the covariance matrix for the fitted parameters.
%
covp=inv(Jstar'*Jstar);
sigmapstar=sqrt(diag(covp));
fprintf(1,'Chi^2=%f, p-value=%f \n',[chi2; pvalue]);
fprintf(1,'Erosion Rate=%f +- %f (mm/kyr) \n',[pstar(1); ...
		    sigmapstar(1)]);
fprintf(1,'10-Be Attenuation length=%f +- %f \n',[pstar(2); ...
		    sigmapstar(2)]);
fprintf(1,'26-Al Attenuation length=%f +- %f \n',[pstar(3); ...
		    sigmapstar(3)]);
%
% Save the results.
%
save calbhcoreboth.mat
