%
% This script plots the results from calClKcore.m
%
%
% Load in the .mat file containing the results.
%
load calClKprofile.mat
%
% Set the attenuation length for spallation production and the
% erosion rate to the best fitting parameters.
%
nominal36(:,12)=pstar(2);
nominal36(:,3)=pstar(1);
%
% Compute predicted 36-Cl using the fitted parameters.
%
maxdepth=150000;
age=10000;
pp=physpars();

N36pred=zeros(size(depths));
N36predc=zeros(size(depths));
for k=1:nsamples
  sp=samppars36(nominal36(k,:));
  sf=scalefacs36(sp);
  cp=comppars36(pp,sp,sf,maxdepth);
  N36predc(k)=predN36depth(pp,sp,sf,cp,age,depths(k),'sa');
  N36pred(k)=N36predc(k)+cp.N36r;
end
%
% Plot the 36-Cl profile.
%
figure(1);
clf;
plot(depths,samples(:,1),'ko')
%errorbar(depths,samples(:,1),E,'ko');
axis([0 2000 0 2500000]);
set(gca,'Yscale','log');
hold on
plot(depths,N36pred,'k.');
ylabel('36-Cl Concentration (atoms/gram)-K');
xlabel('Depth (grams/cm^2)');
print -dpng plotcoreK.png
print -dpdf plotcoreK.pdf
print -deps plotcoreK.eps