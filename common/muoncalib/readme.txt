These notes give procedures for calibrating muon production parameters
fstar and sigma0 for production of 10-Be and 26-Al in quartz and for
production of 36-Cl from Ca and K by muons.  This directory contains
the MATLAB code and input data needed to recreate these results, but
the output from the calibration process is also included in this
directory, so there should be no need to rerun these computations
unless the input data or the underlying production model has been
changed.
 
A more detailed description of the models used here will eventually be
given in a published paper.  For now, please contact the author (Brian
Borchers, borchers@nmt.edu) if you have any questions.

Prerequisites: In order to run this code, you will need access to
MATLAB.  The results in this directory were produced using MATLAB 7.10
(R2010A) Later versions of MATLAB should work, but earlier versions of
MATLAB may well fail.  The code in this directory makes extensive use
of routines from the ../production directory- please make sure that
these are correctly configured for the scaling scheme that you're
interested in.  You must all make sure that the production directory is
in your MATLAB search path, by (e.g.) issuing the command 

>> addpath ../production  

Shell scripts: Some programs in this directory take quite a long time
(e.g. days, not hours) to run.  For this reason it is generally best to run
them unattended on a server rather than a desktop or laptop computer.
I have written shell scripts that can automate this process on a Linux
computer.  If you are doing the computations on a Windows machine then
you'll simply want to run the MATLAB scripts from the command line.
  
1. Calibration of parameters for 10-Be and 26-Al.  

The muon production parameters for 10-Be and 26-Al from quartz are calibrated
from the Beacon Heights profile.  In the calibration process we must estimate
the erosion rate for this site, as well as attenuation lengths for spallation
production of 10-Be and 26-Al.  Because the same erosion rate estimate is used
for both nuclides, the calibration of the production parameters for the two
parameters is tied together and both must be estimated at the same time.  

To run the calibration, either use the shell script "runcalbhcore", or 
directly run the MATLAB script "calbhcore.m" from the MATLAB command line.  
The script will store the results of the calibration run in the file 
calbhcore.mat.  The output from calbhcore.m gives values for the the 
parameters fstar10, sigma010, fstar26, and sigma026.  These values must be
transfered into physpars.m file in ../production.  
 
Because spallation production has a small effect on on the calibration of
muon production, and the muon production parameters have a small effect
on the calibration of spallation production parameters, it is necessary to
recalibrate the spallation production parameters after updating the muon
parameters.  This process is repeated until all of the parameters have 
converged to 3-4 digits.  

Once the spallation and muon production parameters have stabilized, we can
plot the results of the calibration.  This is done by running the script
"plotbealmuons.m".  This script produces two plots, showing the measured 
concentrations (with one-sigma error bars) and model predicted concentrations
of 10-Be and 26-Al through the depth range of the Beacon Heights core.  
These are stored in .eps, .png, and .pdf format.  

In addition to the primary calibration data set, measurements of
samples from the Beacon Heights core were conducted by other investigators.  
These non-CRONUS data are somewhat more scattered than the calibration data
but fit the model reasonably well.  Plots of these secondary data and 
model predictions are produced by the "plotbealothermuons.m" script.  Again, 
the results are stored in .eps, .png, and .pdf format.  

2. Calibration of parameters for 36-Cl production from Ca and K.  

The data for production of 36-Cl by muons from K comes from the
Wyengala quarry profile.  Data for production of 36-Cl by muons from
Ca comes from another quarry profile.  In the case of the Ca profile,
an unmeasured amount of overburden was removed from the top of the
profile, so this overburden has to be estimated along with the erosion
rate, attenuation length, and the muonic production parameters.

The input data are in "calib36Kcore.mat" and "calib36Cacore.mat".  

To run the calibration of the muon production parameters for K, run
the script "calClKprofile.m".  This stores its results in the file
"calClKprofile.mat".  
 
To run the calibration of the muon production parameters for Ca, run
the script "calClCaprofile.m".  This stores its results in the
file "calClCaprofile.mat"
 
As with 10-Be and 26-Cl, it is necessary to iteratively recalibrate
the spallation production parameters after adjusting the muon
production parameters.  Once the parameters have all converged to 3-4
digits, the process is complete.
 
Once the production parameters have converged, run the scripts
"plotCamuons.m" and "plotKmuons.m" to produce plots of the measured
and modeled concentrations.

