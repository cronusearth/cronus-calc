function [Chi2,chi2min,bestER,bestPf0]=calibratePf0(Cl36,Be10,age,Pf0s,erates)
%
%   Objective: 
%
%   To create 3-d grid of chi-square values, in Pf0-eroision rate space
%
%   Usage:
%
%   Chi2=calibratePf0(Cl36,Be10,age,Pf0s,erates)
%
%   Input:
%
%       CL36                profile composistion
%       Be10             vector of concentration uncertanties
%       age                 vector of ages
%       Pf0s              vector of inhers
%       erates               prior distribution
%
%   Output:
%   
%       Chi2                Chi Squared grid w/ location and value of the
%                           minimum

%

m = length(Pf0s);
n = length(erates);
conc36=Cl36.comp(:,1);
conc10=Be10.comp(:,9);
nCl36depths=length(Cl36.depths);
nBe10depths=length(Be10.depths);


%   Build Chi2 for the Pf0 profile
Chi2=zeros(m,n);

for j = 1:m; % loop over Pf0
    fprintf(1,'Now on loop %d of %d\n',[j; m]);
    
    for i = 1:nCl36depths; % loop over depths
        % Compute maximum depth to be called later by prodz36.m 
        maxdepth36 = 100+Cl36.depths(i)+max(erates)/10*age*max(Cl36.comp(:,5));
        [pp,sp36,sf36,cp36]=getpars36cal(Cl36.comp(i,:),maxdepth36,Pf0s(j));
        for k = 1:n
            sp36.epsilon=erates(k);
            Chi2(j,k) = Chi2(j,k)+...
		((conc36(i) - cp36.N36r - predN36depth(pp,sp36,sf36,...
                cp36,age,Cl36.depths(i)))/Cl36.concsig(i))^2;
        end
    end
end


% %   Now build chi2 for the Be10 profile
% Compute maximum depth to be called later by prodz1026.m 
%
% Note that the parameters don't vary with depth!
%
maxdepth1026 = 30000;
[pp,sp1026,sf1026,cp1026]=getpars1026(Be10.comp(1,:), ...
				 maxdepth1026);
for i = 1:nBe10depths;

%
% The following is silly- predN1026 doesn't depend on Pf(0)!
%
%   for j = 1:m; % loop over Pf0
%       for k = 1:n  % loop over the erosion rates
%         sp1026.epsilon=erates(k);  % update the erosion rate
%         Chi2(j,k)=Chi2(j,k)+...
%            ((conc10(i) - predN1026depth(pp,sp1026,sf1026,cp1026,...
%                 age,Be10.depths(i)))/Be10.concsig(i))^2;
%         end
%     end
%
% Instead, just loop over the erosion rates.
%
  for k=1:n % loop over the erosion rates.
    sp1026.epsilon=erates(k);
    tempresid=((conc10(i) - predN1026depth(pp,sp1026,sf1026,cp1026,...
          age,Be10.depths(i)))/Be10.concsig(i))^2;
    for j=1:m
      Chi2(j,k)=Chi2(j,k)+tempresid;
    end
  end
end

% Find mininmum Chi2 solution
[chi2min a]=min(min(Chi2));
[chi2min b]=min(min(Chi2'));
chi2min
bestER=erates(a)
bestPf0=Pf0s(b)
