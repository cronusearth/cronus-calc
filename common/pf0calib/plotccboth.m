%
% Script for plotting the results of the calccboth script.
%
%
% First load in the results from the calccboth script.
%
load calccboth.mat
%
% Work out the predicted 36-Cl from rstar.  Note that this
% includes the radiogenic 36-Cl.  It's vastly faster to compute
% them this way then to rerun getpars36 and predN36depth for each
% sample in the profile. 
%
predictedN36=clsigmas.*rstar(nbesamples+1:end)+clsamples(:,1);
%
% Compute predicted N10 for the range of depths.
%
mydepths=(0.0:10:500)';
[pp,sp,sf,cp]=getpars1026(nominal10(1,:),25000);
sp.epsilon=pstar(1);
age=10000;
[predictedN10,predictedN26]=predN1026depth(pp,sp,sf,cp,age, ...
					   mydepths);
%
% Plot the 10-Be profile.
%
figure(1);
clf;
herrorbar(nominal10(:,9),bedepths,besigmas,'ko');
set(gca,'YDir','reverse');
axis([0 2e6 0 500]); 
hold on
plot(predictedN10,mydepths,'k');
xlabel('10-Be Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng ccbeprofile.png
print -deps ccbeprofile.eps
%
% Plot the 36-Cl profile.
%
figure(2);
clf;
herrorbar(nominal36(:,1),cldepths,clsigmas,'ko');
set(gca,'YDir','reverse');
axis([0 1e6 0 500]); 
hold on
plot(predictedN36,cldepths,'k.');
xlabel('36-Cl Concentration (atoms/gram)');
ylabel('Depth (grams/cm^2)');
print -dpng ccclprofile.png
print -deps ccclprofile.eps
%
% Next, compute a Chi^2 grid for a range of values around the
% optimum.  
%
Cl36.comp=nominal36;
Cl36.concsig=clsigmas;
Cl36.depths=cldepths;
Be10.comp=nominal10;
Be10.conc=Be10.comp(:,9);
Be10.depths=bedepths;
Be10.concsig=besigmas;
%
% Set up a Pf0 and erosion rate grid.
%
Pf0s = linspace(1000,1400,10);
erates = linspace(5,7,10);
%
% Compute the grid of Chi^2 values.
%
chi2grid=calibratePf0(Cl36,Be10,age,Pf0s,erates);
%
% Now, plot a contour plot of the Chi^2 values.
%
figure(3)
clf
[X,Y]=meshgrid(Pf0s,erates);
minchi2=norm(rstar,2)^2;
contour(X,Y,chi2grid',[minchi2+chi2inv(0.68,2) minchi2+chi2inv(0.68,2)],'k',...
    'LineWidth',2)
hold on
contour(X,Y,chi2grid',[minchi2+chi2inv(0.95,2) minchi2+chi2inv(0.95,2)],'k',...
    'LineWidth',2)
hmin = plot(pstar(2),pstar(1),'o');
ht = title('Contour Plot of the \chi^2 Surface');
hx = xlabel('P_{f,(0)}');
hy = ylabel('ER mm/k years');
set([ht hx hy],'FontSize',12)
set(hmin,'Color',[0 0 0],'MarkerFaceColor',[0 0 0],'Marker','o','LineStyle','none')
set(gca,'XMinorTick','on','YMinorTick','on','FontSize',12)
print -dpng cccontour.png
print -deps cccontour.eps
save plotccboth.mat