%
%  [conc,concunc,ccl,cclunc,covar]= ...
%                         spiketoconc(spikeconc,spikeconcunc,spikemass,...
%                                     spikemassunc,A0,A0unc,samplemass,...
%                                     samplemassunc,RS,RSunc,SS,SSunc)
%
%  Computes the nominal 36-Cl concentration and overall Chloride
%  concentration from Primelab style IDMS data.  Also produces the
%  corresponding uncertainties and covariance.
% 
function [conc,concunc,ccl,cclunc,covar]=spiketoconc(spikeconc,spikeconcunc,...
			      spikemass,spikemassunc,...
			      A0,A0unc,...
			      samplemass,samplemassunc,...
			      RS,RSunc, ...
			      SS, SSunc)
%
% First, compute nominal values of Rm and ccl.
%
[Rm,ccl]=spiketorm(spikeconc,spikemass,A0,samplemass,RS,SS);
%
% Then get the nominal 36-Cl concentration.  
%
conc=Rm*ccl*16.98626898;
%
% Now, compute the uncertainties and covariance.
%
concunc=0;
cclunc=0;
covar=0;
%
% First, derivatives with respect to the spike concentration.
%
[Rm2,ccl2]=spiketorm(spikeconc*1.001,spikemass,A0,samplemass,RS,SS);
conc2=Rm2*ccl2*16.98626898;
dconc=(conc2-conc)/(0.001*spikeconc);
concunc=concunc+dconc^2*spikeconcunc^2;
dccl=(ccl2-ccl)/(0.001*spikeconc);
cclunc=cclunc+dccl^2*spikeconcunc^2;
covar=covar+dconc*dccl*spikeconcunc^2;
%
% Next, for the spike mass.
%
[Rm2,ccl2]=spiketorm(spikeconc,spikemass*1.001,A0,samplemass,RS,SS);
conc2=Rm2*ccl2*16.98626898;
dconc=(conc2-conc)/(0.001*spikemass);
concunc=concunc+dconc^2*spikemassunc^2;
dccl=(ccl2-ccl)/(0.001*spikemass);
cclunc=cclunc+dccl^2*spikemassunc^2;
covar=covar+dconc*dccl*spikemassunc^2;
%
% Next, for A0.
%
[Rm2,ccl2]=spiketorm(spikeconc,spikemass,A0*1.000001,samplemass,RS,SS);
conc2=Rm2*ccl2*16.98626898;
dconc=(conc2-conc)/(0.001*A0);
concunc=concunc+dconc^2*A0unc^2;
dccl=(ccl2-ccl)/(0.001*A0);
cclunc=cclunc+dccl^2*A0unc^2;
covar=covar+dconc*dccl*A0unc^2;
%
% Next, for samplemass.
%
[Rm2,ccl2]=spiketorm(spikeconc,spikemass,A0,samplemass*1.001,RS,SS);
conc2=Rm2*ccl2*16.98626898;
dconc=(conc2-conc)/(0.001*samplemass);
concunc=concunc+dconc^2*samplemassunc^2;
dccl=(ccl2-ccl)/(0.001*samplemass);
cclunc=cclunc+dccl^2*samplemassunc^2;
covar=covar+dconc*dccl*samplemassunc^2;
%
% Next, for RS.
%
[Rm2,ccl2]=spiketorm(spikeconc,spikemass,A0,samplemass,RS*1.001,SS);
conc2=Rm2*ccl2*16.98626898;
dconc=(conc2-conc)/(0.001*RS);
concunc=concunc+dconc^2*RSunc^2;
dccl=(ccl2-ccl)/(0.001*RS);
cclunc=cclunc+dccl^2*RSunc^2;
covar=covar+dconc*dccl*RSunc^2;
%
% Next, for SS.
%
[Rm2,ccl2]=spiketorm(spikeconc,spikemass,A0,samplemass,RS,SS*1.001);
conc2=Rm2*ccl2*16.98626898;
dconc=(conc2-conc)/(0.001*SS);
concunc=concunc+dconc^2*SSunc^2;
dccl=(ccl2-ccl)/(0.001*SS);
cclunc=cclunc+dccl^2*SSunc^2;
covar=covar+dconc*dccl*SSunc^2;
%
% Finally, take the sqrts.
%
concunc=sqrt(concunc);
cclunc=sqrt(cclunc);
