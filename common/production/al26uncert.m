%
% uncert=al26uncert(c)
%
% Given a 26-Al concentration in atoms/gram, computes the
% uncertainty based on interpolation of the standard deviations of
% the intercomparison sample A and N uncertainties.
%
% Inputs:
%       c            concentration (atoms/gram)
%
% Output
%       uncert       uncertainty (1-sigma) atoms/gram)
%
function uncert=al26uncert(c)
%
% The concentrations and uncertainties for the A and N samples.
%
% From summary in Phillips et al. (2015) "The CRONUS-Earth Project: A Synthesis"
c0=1.05e6;            % N-sample concentration atoms/gram
u0=100 * 0.11e6 / c0; % 1-sigma as a percentage of c0.
c1=1.43e8;            % A-sample concentration atoms/gram
u1=100 * 0.07e8 / c1; % 1-sigma uncertainty as a percentage.
%
% Interpolate on the log of the concentration.  
% 
u=u0+((log(c)-log(c0))/(log(c1)-log(c0)))*(u1-u0);
uncert=u*c/100;
