
function out=usingattenuationlengthnormal(sample);

%sample has one row per sample and each sample has 4 columns: latitude,
%longitude, elevation, pressure

numbersamples=size(sample,1);
%initialize the results vectors
attenlength=zeros(numbersamples,1);

for i=1:numbersamples;
    attenlength(i)=attenuationlength(sample(i,1),sample(i,2),sample(i,3),sample(i,4));
end
out.attenlength=attenlength;