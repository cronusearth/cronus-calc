function l=rawlambda(pressure,rigiditycutoff,arf)
%
% l=attenuationlength(pressure,rigiditycutoff)
%
% Interpolates an attenuation length for a given atmospheric depth
% (in g/cm^2) and rigidity cutoff.  The attenuation length returned
% is the "flat horizon" Lambdafe.  Further correction may be
% required to account for topography.  
%

%
% Take care of extreme rigidity cutoffs that should really be 0.
%
if (rigiditycutoff < 0)
  rigiditycutoff=0;
end
%
% Convert pressure (hPa) to atmospheric depth (g/cm^2).
%
atmdepth=pressure*1.019716;
%
% Attenuation lengths generated from Sato et al. (2008) model fluxes for
% protons and neutrons. Both fluxes were summed at each (Rc, Atmdepth) point
% and attenuation lengths calculated from the combined fluxes between each
% Atm depth pair. Values at each Atm depth are between that depth and 50 g/cm2
% higher in the atmosphere, for each Rc value.
%
T = [127.6 126.9 126.8 126.9 127.1 127.5 127.9 128.4 129.1 129.9 130.9 132.3 134.0 136.5 139.9
127.6 126.9 126.8 126.9 127.1 127.5 127.9 128.4 129.1 129.9 130.9 132.3 134.0 136.5 139.9
127.4 126.7 126.7 126.9 127.2 127.7 128.3 129.0 129.9 131.0 132.7 134.6 137.1 140.5 145.3
127.5 126.9 127.0 127.3 127.8 128.5 129.3 130.4 131.6 133.2 135.2 137.7 140.9 145.2 151.2
128.0 127.5 127.7 128.2 128.9 129.8 130.9 132.0 133.6 135.5 137.9 140.9 144.8 149.9 157.0
128.9 128.4 128.7 129.4 130.2 131.3 132.5 134.1 135.9 138.1 140.9 144.4 148.8 154.6 162.6
130.2 129.7 130.1 130.8 131.8 133.0 134.5 136.2 138.3 140.8 143.8 147.7 152.6 159.0 167.7
131.6 131.2 131.6 132.4 133.5 134.8 136.4 138.3 140.6 143.3 146.6 150.8 156.0 163.0 172.5
133.0 132.6 133.1 134.0 135.2 136.6 138.3 140.4 142.8 145.7 149.2 153.6 159.2 166.6 176.8
134.4 134.0 134.6 135.6 136.8 138.3 140.1 142.2 144.8 147.8 151.5 156.2 162.1 169.9 180.7
135.9 135.5 136.0 137.0 138.3 139.9 141.8 144.0 146.6 149.8 153.7 158.6 164.7 172.9 184.3
137.3 136.8 137.4 138.5 139.8 141.5 143.4 145.7 148.4 151.7 155.7 160.7 167.2 175.7 187.5
138.6 138.2 138.8 139.8 141.2 142.9 144.9 147.3 150.1 153.4 157.6 162.8 169.4 178.2 190.5
139.8 139.4 140.0 141.1 142.5 144.2 146.3 148.7 151.6 155.0 159.3 164.6 171.4 180.5 193.2
140.9 140.5 141.1 142.2 143.7 145.4 147.5 150.0 152.9 156.4 160.8 166.2 173.2 182.5 195.5
141.8 141.4 142.0 143.2 144.7 146.4 148.5 151.0 154.0 157.6 162.0 167.6 174.7 184.2 197.5
142.6 142.1 142.8 143.9 145.4 147.2 149.4 151.9 154.9 158.6 163.1 168.7 176.0 185.6 199.2
143.1 142.7 143.3 144.5 146.0 147.8 150.0 152.5 155.6 159.3 163.8 169.5 176.9 186.7 200.4
143.6 143.0 143.7 144.9 146.4 148.2 150.4 153.0 156.0 159.8 164.4 170.1 177.5 187.4 201.2
143.9 143.3 143.9 145.1 146.6 148.4 150.6 153.2 156.3 160.0 164.7 170.4 177.9 187.8 201.6
144.2 143.4 144.0 145.1 146.6 148.5 150.7 153.3 156.4 160.1 164.8 170.6 178.0 187.9 201.8];

depths = (1050:-50:350);
cutoffs = (0:1:20)';
%
% interpolate from the table.
%
l=interp2(depths,cutoffs,T,atmdepth,rigiditycutoff).*arf;
%
% Check for NaN.
%
if (isnan(l))
  error('attenuation length returned NaN');
end