function out=usingspiketoconc(spiking)

%spiking is a table with all the appropriate information for spiketoconc is
%in the columns and each row is a sample.  This function just produces
%outputs for all the different samples simultaneously

numbersamples=size(spiking,1);
%initialize the results vectors
conc=zeros(numbersamples,1);
concunc=zeros(numbersamples,1);
ccl=zeros(numbersamples,1);
cclunc=zeros(numbersamples,1);
covar=zeros(numbersamples,1);

for i=1:numbersamples;
    [conc(i),concunc(i),ccl(i),cclunc(i),covar(i)]=spiketoconc(spiking(i,1),spiking(i,2),...
        spiking(i,3),spiking(i,4),spiking(i,5),spiking(i,6),spiking(i,7),...
        spiking(i,8),spiking(i,9),spiking(i,10),spiking(i,11),spiking(i,12));
end
%create a total variable so that I can copy all the stuff out at once

total=zeros(numbersamples,5);
size(total)
size(conc)
total(:,1)=conc(:,1);
total(:,2)=concunc(:,1);
total(:,3)=ccl(:,1);
total(:,4)=cclunc(:,1);
total(:,5)=covar(:,1);

out.conc=conc;
out.concunc=concunc;
out.ccl=ccl;
out.cclunc=cclunc;
out.covar=covar;
out.total=total;