%
% uncert=ne21uncert(c)
%
% Given a 21-Ne concentration in atoms/gram, computes the
% uncertainty in atoms/gram.
%
% Inputs:
%       c            concentration (atoms/gram)
%
% Output
%       uncert       uncertainty (1-sigma) atoms/gram)
%
function uncert=ne21uncert(c)
% From summary in Phillips et al. (2015) "The CRONUS-Earth Project: A Synthesis"
uncert=( 100 * 0.10e8 / 3.48e8 )*c;
