%
% uncert=he3uncert(c)
%
% Given a 3-He concentration in atoms/gram, computes the
% uncertainty in atoms/gram.
%
% Inputs:
%       c            concentration (atoms/gram)
%
% Output
%       uncert       uncertainty (1-sigma) atoms/gram)
%
function uncert=he3uncert(c)
% From summary in Phillips et al. (2015) "The CRONUS-Earth Project: A Synthesis"
uncert=( 100 * 0.06e9 / 5.02e9 )*c;
