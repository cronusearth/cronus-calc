
function [ total, conc36, ratio36, rad36 ] = usingpredN36( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );

%
% Get basic info about the sample ages.
% Needs file with an age for each sample - this needs to be saved in the
% inputfile as 'ages'
%
nsamples=size(nominal36,1);
pp=physpars();
maxage=10000;
const=1000000*35.453/6.022E23;
% 
for i=1:nsamples;
    
  sp=samppars36(nominal36(i,:));
  sf=scalefacs36(sp,scaling_model);
  ppmCl=nominal36(i,36);
  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion (g/cm^2/kyr)+
  % thickness * density + a safety factor.
  %
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  
  cp=comppars36(pp,sp,sf,maxdepth);
  age=ages(i);
  
  % Use deltat of 0.1 for aging and 1 for erosion samples
  conc36(i)=predN36(pp,sp,sf,cp,age,scaling_model,0.1);
  ratio36(i)=conc36(i)*const/ppmCl;

 % rad36(i)=cp.N36r;
 %use option below to get N36r for samples where exposure age=rad age
  rad36(i)=cp.Prtot*(1-exp(-pp.lambda36Cl*age*1000))/pp.lambda36Cl;
end
ratio36=ratio36';
total=zeros(nsamples,1);
total(:,1)=conc36;
total(:,2)=ratio36;
total(:,3)=rad36;


if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
