function [ prod36 ] = usingprodz36( scaling_model, age, inputfilename, outputfilename)

% Calculates the long-term average production rates for
%a batch of samples. age is maxage (in ka) for prod rates. This works for
%samples with erosion rates and should calculate the average long-term
%production rate for what that particular sample has experienced (starts at
%depth and averages the production through time). 

load( inputfilename, '-mat' );
%
% Get basic info about the sample ages.
%
nsamples=size(nominal36,1);
pp=physpars();
maxage=age;
prod36=zeros(nsamples,8);
index=1;
for i=1:nsamples;
    
    sp=samppars36(nominal36(i,:));
    sf=scalefacs36(sp,scaling_model);
  
    % Figure out the maximum possible depth at which we'll ever need a
    % production rate.  This is depthtotop + maxage * erosion (g/cm^2/kyr)+
    % thickness * density + a safety factor.
    %
    maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  
    cp=comppars36(pp,sp,sf,maxdepth);
    %
    % Get the erosion rate in gm/(cm^2*yr) (sp.epsilon is in gm/cm^2/kyr)  
    %
    erosionrate=sp.epsilon/1000;
    %
    % Note that from now on in this function, the erosion rate is in g/cm^2/yr.
    %
    %
    %
    % Convert the sample thickness to g/cm^2.
    thickness=sp.ls*sp.rb;
    % 
    % Find average depth of the sample
    depths=sp.depthtotop+thickness/2;
    % Adjust contemporary depth to depth at start time.
    %
    currentdepths=depths+erosionrate*age*1000;
    %
    % Pick deltat timestep for the integration.  
    %
    deltat=0.1; %0.1 = 100 years
    %
    % We integrate until the point in time where the sample was collected.
    %
    tfinal=sp.tfinal;
    %
    % Figure out the depth spacing (how much it moves up each time step).
    %
    deltadepth=deltat*erosionrate*1000;
    %
    % Now, the main integration loop.
    %
    t=tfinal-age;
    k=1;
    while (t+deltat < tfinal)
        %
        % Update the elevation/latitude scaling factor to the current
        % time.  Note that our t is in kyr, with negative values in the
        % past, while the TDSF stuff is done in terms of years before present. 
        %
        interptime=t+deltat/2;
        sf.currentsf=getcurrentsf(sf,interptime,scaling_model,'cl');
        
        
        % Average production over a number of layers (in case samples are
        % thick)
        
        % Convert the sample thickness to g/cm^2.
        %
        %thickness=sp.ls*sp.rb;
        %
        % Set the number of depths to interpolate production across the
        % thickness of the sample.
        %
        %ndepths=10;
        %
        % Work out the specific depths at which to compute cumulative
        % production.  
        %
        %depths=zeros(ndepths,1);
        %deltadepth=thickness/ndepths;
        %    for i=1:ndepths
        %    depths(i)=sp.depthtotop+deltadepth/2+deltadepth*(i-1);
        %    end
        %
        % Compute the production rate.  We use the mid point of the range
        % of depths corresponding to the current time interval.  
        %
        [Prodtotal(k), prods, ProdsCa(k), ProdsK(k),ProdsTi,ProdsFe,Prodth(k),Prodeth(k),...
        Prodmu,ProdmuCa(k),ProdmuK(k),Phith,Phieth,Kpercent,Capercent,Clpercent]...
        =prodz36(currentdepths-deltadepth/2,pp,sf,cp);
        %
        t=t+deltat;
        k=k+1;
    end 
    % average the prod rates and record for each sample
    prod36(index,1) = mean(Prodtotal);
    prod36(index,2) = mean(ProdsCa);
    prod36(index,3) = mean(ProdsK);
    prod36(index,4) = mean(Prodth);
    prod36(index,5) = mean(Prodeth);
    prod36(index,6) = mean(ProdmuCa);
    prod36(index,7) = mean(ProdmuK);
    prod36(index,8) = cp.N36r;
   
    index=index+1
end

if nargin > 3
  % Save the results.
  %
  save( outputfilename );
end
