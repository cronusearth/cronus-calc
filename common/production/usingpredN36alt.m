
function [ total, conc36 ] = usingpredN36alt( scaling_model, inputfilename, outputfilename )
  
load( inputfilename, '-mat' );
%
% Get basic info about the sample concentration at various ages. This code
% takes a particular sample and runs the concentration for a number of
% ages and stores the concentration for each one. 
%
nsamples=size(nominal36,1);
pp=physpars();
maxage=10000;

for i=1:nsamples;
    
  sp=samppars36(nominal36(i,:));
  sf=scalefacs36(sp,scaling_model);
  
  % Figure out the maximum possible depth at which we'll ever need a
  % production rate.  This is depthtotop + maxage * erosion (g/cm^2/kyr)+
  % thickness * density + a safety factor.
  %
  maxdepth=sp.depthtotop+maxage*sp.epsilon+sp.ls*sp.rb+1000; 
  
  cp=comppars36(pp,sp,sf,maxdepth);
  for j=1:size(ages,2);
  age=ages(j);
  conc36(i,j)=predN36(pp,sp,sf,cp,age,scaling_model,0.1);
  end
end

total=conc36;

if nargin > 2
  % Save the results.
  %
  save( outputfilename );
end
