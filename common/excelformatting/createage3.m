function [nominal3 uncerts3]=createage3(sample)

% Uses the standard input for a helium-3 sample and creates the individual
% variables needed for aging in the rest of the code.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:
%
%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)      
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 3-He concentration (atoms of 3-He/g of target)
%10. Inheritance for 3-He (atoms 3-He/g of target)
%11. Lambdafe (g/cm^2)
%12. Depth to top of sample (g/cm^2)
%13. Year sampled (e.g. 2010)
%14-26. Uncertainty for each of the above variables

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results
nominal3=zeros(numbersamps,13);
uncerts3=zeros(numbersamps,13);

nominal3(:,1:13)=sample(:,1:13);
uncerts3(:,1:13)=sample(:,14:26);

%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal3(:,8)=nominal3(:,8).*nominal3(:,6)./10;
uncerts3(:,8)=uncerts3(:,8).*nominal3(:,6)./10;

%save testsample3 nominal3 uncerts3