function [nominal10 uncerts10 nominal26 uncerts26] = createage1026(sample)

% Uses the standard input for a Be-10/Al-26 sample and creates the individual
% variables needed for aging in the rest of the code.  For calibration 
% datasets, use createcalib1026.m.  

%sample input must be the inputs
%with no spaces between: inputs,uncerts,independent ages. 
% One row per sample with the following columns:

%1. Latitude (decimal degrees, -90(S) to +90(N))
%2. Longitude (decimal degrees, 0-360 degrees east)
%3. Elevation (meters)
%4. Pressure (hPa)       
%5. sample thickness (cm)
%6. bulk density (g/cm^3)
%7. Shielding factor for terrain, snow, etc. (unitless)
%8. Erosion-rate epsilon (mm/kyr)
%9. Sample 10-Be concentration (atoms of 10-Be/g of target)
%10. Sample 26-Al concentration (atoms of 26-Al/g of target)
%11. Inheritance for Be (atoms 10-Be/g of target)
%12. Inheritance for Al (atoms 26-Al/g of target)
%13. Lambdafe (g/cm^2)
%14. Depth to top of sample (g/cm^2)
%15. Year sampled (e.g. 2010)
%16-30. One-sigma uncertainties for each of the above inputs (1-15). 

numbersamps=size(sample,1); %this gives the number of samples
%initialize the results

%go through each sample and see if it has Be and/or Al nd put it into the
%appropriate vectors (sample10 and sample26)
count10=1;
count26=1;

for i=1:numbersamps;

    if isnan(sample(i,9)) || sample(i,9)==0;
    else
        sample10(count10,:)=sample(i,:);
        count10=count10+1;
    end
    if isnan(sample(i,10)) || sample(i,10)==0; 
    else
        sample26(count26,:)=sample(i,:);
        count26=count26+1;
    end
end

if count10~=1
nominal10(:,1:15)=sample10(:,1:15);
uncerts10(:,1:15)=sample10(:,16:30);
%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal10(:,8)=nominal10(:,8).*nominal10(:,6)./10;
uncerts10(:,8)=uncerts10(:,8).*nominal10(:,6)./10;
end

if count26~=1
nominal26(:,1:15)=sample26(:,1:15);
uncerts26(:,1:15)=sample26(:,16:30);
%convert erosion rate from mm/kyr to g/cm^2/kyr
nominal26(:,8)=nominal26(:,8).*nominal26(:,6)./10;
uncerts26(:,8)=uncerts26(:,8).*nominal26(:,6)./10;
end

%save testsamples1026 nominal10 uncerts10 nominal26 uncerts26